﻿#nullable enable
using NAudio.Wave;
using System.Buffers;
using System.IO.Pipelines;

namespace AudioPlugins
{

    public class AudioStreamReader : WaveStream
    {
        private WaveFormat format;
        private PipeReader pipeReader;
        private long position;

        public AudioStreamReader(PipeReader pipeReader, WaveFormat format)
        {
            this.pipeReader = pipeReader;
            this.format = format;
        }

        public override WaveFormat WaveFormat => format;

        public override long Length => throw new NotSupportedException();

        public override long Position
        {
            get => position;
            set => throw new NotSupportedException();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (pipeReader.TryRead(out var result))
            {
                var bufferSequence = result.Buffer;
                var bytesRead = (int)Math.Min(bufferSequence.Length, count);
                bufferSequence.Slice(0, bytesRead).CopyTo(new Span<byte>(buffer, offset, bytesRead));
                pipeReader.AdvanceTo(bufferSequence.GetPosition(bytesRead));
                position += bytesRead;
                return bytesRead;
            }
            else
            {
                Array.Clear(buffer, offset, count);
                return count;
            }
        }
    }
    public class MicrophoneCapture : IDisposable
    {
        public enum StateType
        {
            Recording,
            Pause,
            Stopped,
        }
        private StateType state;
        private IWaveIn waveIn;
        private Pipe pipe;
        public WaveFormat WaveFormat => waveIn.WaveFormat;

        public MicrophoneCapture(WaveFormat format)
        {
            state = StateType.Pause;
            waveIn = new WaveInEvent();
            waveIn.WaveFormat = format;
            pipe = new Pipe();

            waveIn.DataAvailable += WaveIn_DataAvailable;
        }

        public void StartCapture(bool autoPlay = true)
        {
            if (autoPlay)
            {
                state = StateType.Recording;
            }
            waveIn.StartRecording();
        }
        public void Resume() => state = StateType.Recording;
        public void Pause() => state = StateType.Pause;

        public void StopCapture()
        {
            state = StateType.Stopped;
            waveIn.StopRecording();
            pipe.Writer.Complete();
        }

        public PipeReader GetAudioPipeReader()
        {
            return pipe.Reader;
        }

        private void WaveIn_DataAvailable(object? sender, WaveInEventArgs e)
        {
            if (state == StateType.Pause || state == StateType.Stopped)
            {
                if (pipe.Writer.UnflushedBytes > 0)
                {
                    pipe.Writer.FlushAsync();
                }
            }
            else if (state == StateType.Recording)
            {
                pipe.Writer.Write(e.Buffer);
                if (pipe.Writer.UnflushedBytes > 2048)
                {
                    pipe.Writer.FlushAsync();
                }
            }
        }

        public void Dispose()
        {
            waveIn.Dispose();
            pipe.Writer.Complete();
            pipe.Reader.Complete();
        }
    }
    public class AudioPlayer : IDisposable
    {
        private WaveOutEvent waveOut;
        private IWaveProvider? provider;
        private EventWaitHandle waitHandle;

        public AudioPlayer()
        {
            provider = null;
            waveOut = new WaveOutEvent();
            waitHandle = new EventWaitHandle(true, EventResetMode.ManualReset);
            waveOut.PlaybackStopped += (object? sender, StoppedEventArgs e) =>
            {
                waitHandle.Set();
            };
        }

        public void Play(IWaveProvider provider)
        {
            if (waveOut != null && provider != null)
            {
                waveOut.Init(provider);
                waitHandle.Reset();
                waveOut.Play();
                this.provider = provider;
            }
        }

        public void WaitForCompletation()
        {
            waitHandle.WaitOne();
        }

        public async Task WaitForCompletationAsync(Action<float>? updateProgressCallback = null, long? length = null)
        {
            while (waveOut.PlaybackState != PlaybackState.Stopped)
            {
                await Task.Delay(100);
                if (updateProgressCallback != null && length != null)
                {
                    updateProgressCallback?.Invoke(waveOut.GetPosition() * 1f / length.Value);
                }
            }
        }

        public PlaybackState State => waveOut.PlaybackState;

        public void StopPlayback()
        {
            if (waveOut != null && waveOut.PlaybackState == PlaybackState.Playing)
            {
                waveOut.Stop();
            }
        }

        private bool _Disposed = false;
        public void Dispose()
        {
            if (!_Disposed)
            {
                _Disposed = true;
                waveOut.Dispose();
                waitHandle.Set();
                waitHandle.Dispose();
            }
        }


        // =============== Examples ======================
        public static async Task PlayMP3(Stream stream, Action<float>? updateProgressCallback = null)
        {
            var provider = new Mp3FileReader(stream);
            var player = new AudioPlayer();
            player.Play(provider);
            await player.WaitForCompletationAsync(updateProgressCallback, provider.Length);
        }
        public static async Task PlayWAV(Stream stream, Action<float>? updateProgressCallback = null)
        {
            var provider = new WaveFileReader(stream);
            var player = new AudioPlayer();
            player.Play(provider);
            await player.WaitForCompletationAsync(updateProgressCallback, provider.Length);
        }
        public static async Task PlayPCM(byte[] data, WaveFormat format, Action<float>? updateProgressCallback = null)
        {
            var provider = new BufferedWaveProvider(format);
            provider.AddSamples(data, 0, data.Length);
            var player = new AudioPlayer();
            player.Play(provider);
            await player.WaitForCompletationAsync(updateProgressCallback, data.Length);
        }

        public async Task PlayAudio<Provider>(Provider provider) where Provider : IWaveProvider
        {
            var player = new AudioPlayer();
            player.Play(provider);
            await player.WaitForCompletationAsync();
        }
        public async Task RecordAndPlayLoopback(int rate, int bits)
        {
            var fmt = new WaveFormat(rate, bits, 1);
            var capture = new MicrophoneCapture(fmt);
            capture.StartCapture();
            var provider = new AudioStreamReader(capture.GetAudioPipeReader(), capture.WaveFormat);
            await PlayAudio(provider);
        }
    }

}