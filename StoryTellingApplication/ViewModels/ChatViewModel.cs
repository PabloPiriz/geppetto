﻿using ChatGPT.Models;
using Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace StoryTellingApplication.ViewModels
{
    public enum StateView
    {
        UserInput,
        Generating,
        Playing,
    }

    public class ChatViewModel : ViewModelBase
    {
        private ICommand? _sendCommand;
        public ICommand SendCommand => _sendCommand ??= new RelayCommand(async (p) => await Send(p));
        
        private ICommand? _generateCommand;
        public ICommand GenerateCommand => _generateCommand ??= new RelayCommand(async (p) => await Generate());
        
        private ICommand? _editChatItemCommand;
        public ICommand EditChatItemCommand => _editChatItemCommand ??= new RelayCommand((p) => EditChatItem(p));

        private ICommand? _cancelEditChatItemCommand;
        public ICommand CancelEditChatItemCommand => _cancelEditChatItemCommand ??= new RelayCommand((p) => CancelEditChatItem());

        private ICommand? _regenerateChatItemCommand;
        public ICommand RegenerateChatItemCommand => _regenerateChatItemCommand ??= new RelayCommand(async (p) => await RegenerateChatItem(p));

        private ICommand? _playChatItemCommand;
        public ICommand PlayChatItemCommand => _playChatItemCommand ??= new RelayCommand(async (p) => await PlayChatItem(p));

        private ICommand? _saveChatCommand;
        public ICommand SaveChatCommand => _saveChatCommand ??= new RelayCommand(async (w) => await SaveChat(w));

        private ICommand? _loadChatCommand;
        public ICommand LoadChatCommand => _loadChatCommand ??= new RelayCommand(async (w) => await LoadChat(w));

        #region Commands

        private async Task Send(object? p)
        {
            if (string.IsNullOrEmpty(InputText) || string.IsNullOrEmpty(InputRole))
                return;

            var chatItem = new ChatItem
            {
                Role = InputRole,
                Content = InputText
            };

            if (EditingChatItem != null)
            {
                var editIdx = ChatItems.IndexOf(EditingChatItem);
                if (editIdx == -1)
                    throw new InvalidOperationException();
                EditingChatItem = null;
                TruncateChatItemList(editIdx);
                ChatItems.Add(chatItem);
                return;
            }

            ChatItems.Add(chatItem);
            InputText = "";

            if (InputRole == ChatRoleType.system.ToString())
                return;

            await Generate();
        }
        private async Task Generate()
        {
            if (string.IsNullOrEmpty(Model) || !ChatItems.Any())
                return;

            State = StateView.Generating;
            var response = await Engine.Instance.ProcessGPT(new Chat { Model = Model, Messages = ChatItems.ToList() });
            ChatItems.Add(response);
            State = StateView.UserInput;
        }

        private void EditChatItem(object? p) => EditingChatItem = p as ChatItem ?? throw new InvalidCastException();
        private void CancelEditChatItem() => EditingChatItem = null;
        private async Task RegenerateChatItem(object? p)
        {
            var item = p as ChatItem ?? throw new InvalidCastException();
            var itemIdx = ChatItems.IndexOf(item);
            if (itemIdx == -1)
                throw new InvalidOperationException();

            TruncateChatItemList(itemIdx);
            await Generate();
        }
        private Dictionary<string, byte[]> SynthesizedCache { get; } = new Dictionary<string, byte[]>();
        private async Task PlayChatItem(object? p)
        {
            var item = p as ChatItem ?? throw new InvalidCastException();
            State = StateView.Playing;

            // Cache
            using var memory = new MemoryStream();
            if (SynthesizedCache.TryGetValue(item.Uid, out var value))
            {
                memory.Write(value, 0, value.Length);
            }
            else
            {
                await Engine.Instance.AudioManager.SynthesizeText(item.Content, memory, default);
                memory.Position = 0;
                SynthesizedCache.Add(item.Uid, memory.ToArray());
            }
            memory.Position = 0;

            await Engine.Instance.AudioManager.Play(memory, AudioCodecType.MP3, null);
            State = StateView.UserInput;
        }

        // TODO: use shared chat model
        private async Task SaveChat(object? w)
        {
            Microsoft.Win32.SaveFileDialog dlg = new()
            {
                FileName = $"Save_{Guid.NewGuid()}",
                DefaultExt = ".chat_save",
                Filter = "Text documents (.chat_save)|*.chat_save"
            };

            // Show save file dialog box
            bool? result = dlg.ShowDialog(w as Window);

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string file = dlg.FileName;
                using var fileStream = new FileStream(file, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
                await JsonSerializer.SerializeAsync(fileStream, ChatItems.ToList(), new JsonSerializerOptions { WriteIndented = true });
            }
        }
        private async Task LoadChat(object? w)
        {
            Microsoft.Win32.OpenFileDialog dlg = new()
            {
                Filter = "Text documents (.chat_save)|*.chat_save"
            };

            // Show open file dialog box
            bool? result = dlg.ShowDialog(w as Window);

            // Process read file dialog box results
            if (result == true)
            {
                if (ChatItems.Any())
                {
                    var overrideChat = MessageBox.Show("Replace current chat?", "Load", MessageBoxButton.YesNo);
                    if (overrideChat != MessageBoxResult.Yes)
                        return;
                }
                // Read document
                string file = dlg.FileName;
                using var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
                var chatItems = (await JsonSerializer.DeserializeAsync(fileStream, typeof(List<ChatItem>))) as List<ChatItem>;
                if (chatItems != null)
                {
                    TruncateChatItemList(0);
                    chatItems.ForEach(i => ChatItems.Add(i));
                }
            }
        }
        #endregion

        private StateView _state = StateView.UserInput;
        public StateView State
        {
            get => _state;
            private set
            {
                if (State == value)
                    return;

                _state = value;
                OnPropertyChanged(nameof(State));
            }
        }

        public string Model { get; set; } = Chat.MODEL_V35;

        public ObservableCollection<ChatItem> ChatItems { get; } = new ObservableCollection<ChatItem>();
        private void TruncateChatItemList(int startIdx)
        {
            if (startIdx == 0)
                ChatItems.Clear();

            while(startIdx < ChatItems.Count)
            {
                ChatItems.RemoveAt(ChatItems.Count - 1);
            }
        }

        private ChatItem? _editingChatItem;
        public ChatItem? EditingChatItem
        {
            get => _editingChatItem;
            set
            {
                if (_editingChatItem == value)
                    return;

                _editingChatItem = value;
                InputRole = _editingChatItem?.Role ?? Roles.First();
                InputText = _editingChatItem?.Content ?? "";
                OnPropertyChanged(nameof(EditingChatItem));
            }
        }

        public List<string> Roles { get; } = new() {
            $"{ChatRoleType.user}",
            $"{ChatRoleType.system}",
            $"{ChatRoleType.assistant}",
        };

        private string? _inputRole;
        public string? InputRole
        {
            get => _inputRole;
            set
            {
                if (_inputRole == value)
                    return;

                _inputRole = value;
                OnPropertyChanged(nameof(InputRole));
            }
        }

        private string _inputText = "";
        public string InputText
        {
            get => _inputText;
            set
            {
                if (_inputText == value)
                    return;

                _inputText = value;
                OnPropertyChanged(nameof(InputText));
            }
        }

        public ChatViewModel()
        {
            InputRole = Roles.First();
        }
    }
}
