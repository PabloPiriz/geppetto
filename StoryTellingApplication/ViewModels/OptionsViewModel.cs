﻿using Shared;
using StoryTellingApplication.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace StoryTellingApplication.ViewModels
{
    public class OptionsViewModel : ViewModelBase
    {
        private string _AWSTranscribeKey = "";
        public string AWSTranscribeKey
        {
            get => _AWSTranscribeKey;
            set
            {
                if (value == _AWSTranscribeKey)
                    return;

                _AWSTranscribeKey = value;
                OnPropertyChanged(nameof(AWSTranscribeKey));
            }
        }
        private string _AWSTranscribeSecretKey = "";
        public string AWSTranscribeSecretKey
        {
            get => _AWSTranscribeSecretKey;
            set
            {
                if (value == _AWSTranscribeSecretKey)
                    return;

                _AWSTranscribeSecretKey = value;
                OnPropertyChanged(nameof(AWSTranscribeSecretKey));
            }
        }
        public IEnumerable<string> AWSTranscribeRegions => Amazon.RegionEndpoint.EnumerableAllRegions.Select(r => r.SystemName);
        private string _AWSTranscribeRegion = "";
        public string AWSTranscribeRegion
        {
            get => _AWSTranscribeRegion;
            set
            {
                if (value == _AWSTranscribeRegion)
                    return;

                _AWSTranscribeRegion = value;
                OnPropertyChanged(nameof(AWSTranscribeRegion));
            }
        }
        public IEnumerable<string> AWSTranscribeLanguages => new string[] { "es-US" };
        private string _AWSTranscribeLanguage = "es-US";
        public string AWSTranscribeLanguage
        {
            get => _AWSTranscribeLanguage;
            set
            {
                if (value == _AWSTranscribeLanguage)
                    return;

                _AWSTranscribeLanguage = value;
                OnPropertyChanged(nameof(AWSTranscribeLanguage));
            }
        }


        public string[] SynthesizerLanguageCodes => new string[] { "es-ES" };

        private string _SynthesizerLanguageCode = "es-ES";
        public string SynthesizerLanguageCode
        {
            get => _SynthesizerLanguageCode;
            set
            {
                if (value == _SynthesizerLanguageCode)
                    return;

                _SynthesizerLanguageCode = value;
                OnPropertyChanged(nameof(SynthesizerLanguageCode));
            }
        }

        private string _SynthesizerVoiceID = "";
        public string SynthesizerVoiceID
        {
            get => _SynthesizerVoiceID;
            set
            {
                if (value == _SynthesizerVoiceID)
                    return;

                _SynthesizerVoiceID = value;
                OnPropertyChanged(nameof(SynthesizerVoiceID));
            }
        }

        private string _SynthesizerAzureKey = "";
        public string SynthesizerAzureKey
        {
            get => _SynthesizerAzureKey;
            set
            {
                if (value == _SynthesizerAzureKey)
                    return;

                _SynthesizerAzureKey = value;
                OnPropertyChanged(nameof(SynthesizerAzureKey));
            }
        }

        private int _AudioSampleRate = 16000;
        public int AudioSampleRate
        {
            get => _AudioSampleRate;
            set
            {
                if (value == _AudioSampleRate)
                    return;

                _AudioSampleRate = value;
                OnPropertyChanged(nameof(AudioSampleRate));
            }
        }
        private int _AudioBitsPerSample = 16;
        public int AudioBitsPerSample
        {
            get => _AudioBitsPerSample;
            set
            {
                if (value == _AudioBitsPerSample)
                    return;

                _AudioBitsPerSample = value;
                OnPropertyChanged(nameof(AudioBitsPerSample));
            }
        }
        private bool _AudioIsStereo = false;
        public bool AudioIsStereo
        {
            get => _AudioIsStereo;
            set
            {
                if (value == _AudioIsStereo)
                    return;

                _AudioIsStereo = value;
                OnPropertyChanged(nameof(AudioIsStereo));
            }
        }
        private TimeSpan _AudioChunkSize = TimeSpan.FromMilliseconds(100);
        public TimeSpan AudioChunkSize
        {
            get => _AudioChunkSize;
            set
            {
                if (value == _AudioChunkSize)
                    return;

                _AudioChunkSize = value;
                OnPropertyChanged(nameof(AudioChunkSize));
            }
        }

        private string _OpenAIKey = "";
        public string OpenAIKey
        {
            get => _OpenAIKey;
            set
            {
                if (value == _OpenAIKey)
                    return;

                _OpenAIKey = value;
                OnPropertyChanged(nameof(OpenAIKey));
            }
        }

        private bool _AutoSend = false;
        public bool AutoSend
        {
            get => _AutoSend;
            set
            {
                if (value == _AutoSend)
                    return;

                _AutoSend = value;
                OnPropertyChanged(nameof(AutoSend));
            }
        }

        public OptionsViewModel(bool load = false)
        {
            if (load) 
                Load();
        }
        public void Load()
        {
            _AWSTranscribeKey = Settings.Default.AWSTranscribeKey;
            // _AWSTranscribeSecretKey = Settings.Default.AWSTranscribeSecretKey;
            _AWSTranscribeRegion = Settings.Default.AWSTranscribeRegion;
            _AWSTranscribeLanguage = Settings.Default.AWSTranscribeLanguage;

            // _SynthesizerAzureKey = Settings.Default.SynthesizerAzureKey;
            _SynthesizerLanguageCode = Settings.Default.SynthesizerLanguageCode;
            _SynthesizerVoiceID = Settings.Default.SynthesizerVoiceID;

            _AudioSampleRate = Settings.Default.AudioSampleRate;
            _AudioIsStereo = Settings.Default.AudioIsStereo;
            _AudioChunkSize = Settings.Default.AudioChunkSize;

            // _OpenAIKey = Settings.Default.OpenAIKey;

            _AutoSend = Settings.Default.AutoSend;
        }

        public void Save()
        {
            Settings.Default.AWSTranscribeKey = _AWSTranscribeKey;
            if (!string.IsNullOrWhiteSpace(_AWSTranscribeSecretKey))
                Settings.Default.AWSTranscribeSecretKey = Cryptography.Protect(_AWSTranscribeSecretKey);
            Settings.Default.AWSTranscribeRegion = _AWSTranscribeRegion;
            Settings.Default.AWSTranscribeLanguage = _AWSTranscribeLanguage;

            if (!string.IsNullOrWhiteSpace(_SynthesizerAzureKey))
                Settings.Default.SynthesizerAzureKey = Cryptography.Protect(_SynthesizerAzureKey);
            Settings.Default.SynthesizerLanguageCode = _SynthesizerLanguageCode;
            Settings.Default.SynthesizerVoiceID = _SynthesizerVoiceID;

            Settings.Default.AudioSampleRate = _AudioSampleRate;
            Settings.Default.AudioIsStereo = _AudioIsStereo;
            Settings.Default.AudioChunkSize = _AudioChunkSize;

            if (!string.IsNullOrWhiteSpace(_OpenAIKey))
                Settings.Default.OpenAIKey = Cryptography.Protect(_OpenAIKey);

            Settings.Default.AutoSend = _AutoSend;

            Settings.Default.Save();
        }

    }
}
