﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StoryTellingApplication.Views;

namespace StoryTellingApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ICommand? _OptionCommand = null;
        public ICommand OptionCommand => _OptionCommand ??= new RelayCommand((p) => ShowOptionWindow());
        private ICommand? _SaveCommand = null;
        public ICommand SaveCommand => _SaveCommand ??= new RelayCommand((p) => ChatView.Model.SaveChatCommand?.Execute(this));
        private ICommand? _LoadCommand = null;
        public ICommand LoadCommand => _LoadCommand ??= new RelayCommand((p) => ChatView.Model.LoadChatCommand?.Execute(this));

        internal void ShowOptionWindow()
        {
            var model = new ViewModels.OptionsViewModel(load: true);
            var options = new OptionsView(model);
            options.Owner = this;
            bool? result = options.ShowDialog();
            if (result == true)
            {
                model.Save();
                Engine.Instance.AudioManager.LoadFromSettings();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
        }
    }
}
