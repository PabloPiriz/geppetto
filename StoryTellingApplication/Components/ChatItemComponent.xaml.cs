﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StoryTellingApplication.Components
{
    /// <summary>
    /// Interaction logic for ChatItemComponent.xaml
    /// </summary>
    public partial class ChatItemComponent : UserControl
    {
        /// <summary>
        /// Registers a dependency property as backing store for the RoleText property
        /// </summary>
        public static readonly DependencyProperty RoleTextProperty =
            DependencyProperty.Register("RoleText", typeof(string), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the ContentText property
        /// </summary>
        public static readonly DependencyProperty ContentTextProperty =
            DependencyProperty.Register("ContentText", typeof(string), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the EditCommand property
        /// </summary>
        public static readonly DependencyProperty EditCommandProperty =
            DependencyProperty.Register("EditCommand", typeof(ICommand), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the EditCommandParameter property
        /// </summary>
        public static readonly DependencyProperty EditCommandParameterProperty =
            DependencyProperty.Register("EditCommandParameter", typeof(object), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the RegenerateCommand property
        /// </summary>
        public static readonly DependencyProperty RegenerateCommandProperty =
            DependencyProperty.Register("RegenerateCommand", typeof(ICommand), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the RegenerateCommandParameter property
        /// </summary>
        public static readonly DependencyProperty RegenerateCommandParameterProperty =
            DependencyProperty.Register("RegenerateCommandParameter", typeof(object), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the PlayCommand property
        /// </summary>
        public static readonly DependencyProperty PlayCommandProperty =
            DependencyProperty.Register("PlayCommand", typeof(ICommand), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        /// <summary>
        /// Registers a dependency property as backing store for the PlayCommandParameter property
        /// </summary>
        public static readonly DependencyProperty PlayCommandParameterProperty =
            DependencyProperty.Register("PlayCommandParameter", typeof(object), typeof(ChatItemComponent),
            new FrameworkPropertyMetadata(null,
                  FrameworkPropertyMetadataOptions.AffectsRender |
                  FrameworkPropertyMetadataOptions.AffectsParentMeasure));

        public string RoleText
        {
            get => (string)GetValue(RoleTextProperty);
            set => SetValue(RoleTextProperty, value);
        }

        public string ContentText
        {
            get => (string)GetValue(ContentProperty);
            set => SetValue(ContentProperty, value);
        }

        public ICommand EditCommand
        {
            get => (ICommand)GetValue(EditCommandProperty);
            set => SetValue(EditCommandProperty, value);
        }
        public object EditCommandParameter
        {
            get => GetValue(EditCommandParameterProperty);
            set => SetValue(EditCommandParameterProperty, value);
        }

        public ICommand RegenerateCommand
        {
            get => (ICommand)GetValue(RegenerateCommandProperty);
            set => SetValue(RegenerateCommandProperty, value);
        }
        public object RegenerateCommandParameter
        {
            get => GetValue(RegenerateCommandParameterProperty);
            set => SetValue(RegenerateCommandParameterProperty, value);
        }

        public ICommand PlayCommand
        {
            get => (ICommand)GetValue(PlayCommandProperty);
            set => SetValue(PlayCommandProperty, value);
        }
        public object PlayCommandParameter
        {
            get => GetValue(PlayCommandParameterProperty);
            set => SetValue(PlayCommandParameterProperty, value);
        }

        public ChatItemComponent()
        {
            InitializeComponent();
        }
    }
}
