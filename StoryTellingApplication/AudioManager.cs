﻿using Amazon.Runtime;
using Amazon.TranscribeStreamingService;
using AudioPlugins;
using AudioSynthesizer.Models;
using ChatGPT.Models;
using ChatGPT;
using NAudio.Wave;
using Shared;
using System.Diagnostics;
using System.Text;
using System.Buffers;
using AudioSynthesizer;
using Microsoft.CognitiveServices.Speech;
using System.Threading.Tasks;
using System;
using System.Threading;
using System.Linq;
using System.IO;
using StoryTellingApplication.Properties;

namespace StoryTellingApplication
{
    public class InvalidSettingException<T> : Exception
    {
        public InvalidSettingException(string setting)
            : base($"Setting value exception '{setting}'")
        { }
        public InvalidSettingException(string setting, T value) 
            : base($"Setting value exception '{setting}': {value}") 
        { }
    }

    public enum AudioCodecType
    {
        MP3,
        WAV,
        PCM,
    }
    internal class AudioManagerSettings
    {
        public string? TranscribeKey { get; set; }
        public string? TranscribeSecretKey { get; set; }
        public string? TranscribeRegion { get; set; }
        public string? TranscribeLanguage { get; set; }

        public int AudioSampleRate { get; set; }
        public int AudioBps { get; set; }
        public bool AudioStereo { get; set; }
        public TimeSpan AudioChunkSize { get; set; }

        public string? SynthesizerApiKey { get; set; }
        public string? SynthesizerLanguageCode { get; set; }
        public string? SynthesizerVoiceID { get; set; }

        public AudioManagerSettings Clone()
        {
            return new AudioManagerSettings()
            {
                TranscribeKey = this.TranscribeKey,
                TranscribeSecretKey = this.TranscribeSecretKey,
                TranscribeRegion = this.TranscribeRegion,
                TranscribeLanguage = this.TranscribeLanguage,

                AudioSampleRate = this.AudioSampleRate,
                AudioBps = this.AudioBps,
                AudioStereo = this.AudioStereo,
                AudioChunkSize = this.AudioChunkSize,

                SynthesizerApiKey = this.SynthesizerApiKey,
                SynthesizerLanguageCode = this.SynthesizerLanguageCode,
                SynthesizerVoiceID = this.SynthesizerVoiceID,
            };
        }
    }
    public class AudioManager
    {
        private AudioManagerSettings Settings { get; set; } = new AudioManagerSettings();

        internal AudioManager()
        {
        }

        internal void LoadSettings(AudioManagerSettings settings)
        {
            Settings = settings.Clone();
        }

        public async Task<string> StartTranscribe(Action<string, string> transcriptionUpdate, CancellationToken cancellationToken)
        {
            Logger.Info($"Starting transcribe system: language {Settings.TranscribeLanguage} region {Settings.TranscribeRegion} chunkSizeMS {Settings.AudioChunkSize}");

            if (Settings.TranscribeKey == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.TranscribeKey), Settings.TranscribeKey);
            if (Settings.TranscribeSecretKey == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.TranscribeSecretKey), Settings.TranscribeSecretKey);
            if (Settings.TranscribeRegion == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.TranscribeRegion), Settings.TranscribeRegion);
            if (Settings.TranscribeLanguage == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.TranscribeLanguage), Settings.TranscribeLanguage);
            if (Settings.AudioChunkSize < TimeSpan.FromMilliseconds(20))
                throw new InvalidSettingException<TimeSpan>(nameof(AudioManagerSettings.AudioChunkSize), Settings.AudioChunkSize);

            // Transcribe
            var transcription = new StringBuilder();
            var transcribeClient = new AmazonTranscribeStreamingClient(
                Settings.TranscribeRegion,
                new Config("pcm", $"{Settings.AudioSampleRate}", Settings.TranscribeLanguage)
                {
                    EnableChannelIdentification = Settings.AudioStereo ? "true" : null,
                    NumberOfChannels = Settings.AudioStereo ? "2" : null,
                }, 
                new BasicAWSCredentials(Settings.TranscribeKey, Settings.TranscribeSecretKey)
            );
            transcribeClient.TranscriptEvent += delegate (object? sender, TranscriptEvent ev)
            {
                var transcript = ev.Transcript?.Results?.FirstOrDefault();
                var message = transcript?.Alternatives?.FirstOrDefault()?.Transcript;
                if (transcript == null || message == null)
                    return;

                if (!transcript.IsPartial)
                {
                    transcription.Append(message);
                    message = "";
                }

                transcriptionUpdate?.Invoke(transcription.ToString(), message);
            };
            transcribeClient.TranscriptException += delegate (object? sender, TranscribeException ex)
            {
                Logger.Error(ex?.Message ?? nameof(TranscribeException));
            };

            // Capture
            Logger.Info($"Starting audio system: rate {Settings.AudioSampleRate} bps {Settings.AudioBps} stereo {Settings.AudioStereo}");
            var audioCapture = new MicrophoneCapture(new WaveFormat(Settings.AudioSampleRate, Settings.AudioBps, Settings.AudioStereo ? 2 : 1));
            audioCapture.StartCapture(autoPlay: false);

            // Pipeline
            await transcribeClient.StartStreaming();
            audioCapture.Resume();
            var reader = audioCapture.GetAudioPipeReader();
            int chunkSize = Settings.AudioSampleRate * (Settings.AudioStereo ? 2 : 1) / (1000 / ((int) Settings.AudioChunkSize.TotalMilliseconds));
            try
            {
                var chunk = new byte[chunkSize];
                while (!cancellationToken.IsCancellationRequested)
                {
                    Array.Clear(chunk, 0, chunkSize);
                    var result = await reader.ReadAtLeastAsync(chunkSize, cancellationToken);
                    Debug.Assert(result.Buffer.Length >= chunkSize);

                    var bufferSequence = result.Buffer;
                    bufferSequence.Slice(0, chunkSize).CopyTo(new Span<byte>(chunk, 0, chunkSize));
                    reader.AdvanceTo(bufferSequence.GetPosition(chunkSize));

                    transcribeClient.StreamBuffer(chunk);
                }
            }
            catch (TaskCanceledException)
            {
            }
            audioCapture.Pause();
            await transcribeClient.StopStreaming();
            return transcription.ToString();
        }


        public async Task SynthesizeText(string text, Stream output, CancellationToken cancellationToken)
        {
            if (Settings.SynthesizerApiKey == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.SynthesizerApiKey), Settings.SynthesizerApiKey);
            if (Settings.SynthesizerLanguageCode == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.SynthesizerLanguageCode), Settings.SynthesizerLanguageCode);
            if (Settings.SynthesizerVoiceID == null)
                throw new InvalidSettingException<string?>(nameof(AudioManagerSettings.SynthesizerVoiceID), Settings.SynthesizerVoiceID);

            using var Synthesizer = new AzureSynthesizerClient(
                Settings.SynthesizerApiKey, 
                "eastus", 
                AzureSynthesizerClient.InputFormatType.Text, 
                SpeechSynthesisOutputFormat.Audio16Khz64KBitRateMonoMp3
            );
            var voices = await Synthesizer.GetVoices();
            await Synthesizer.Synthesize(new SynthesizeJob
            {
                Language = Settings.SynthesizerLanguageCode,
                Voice = voices.Single(v => v.ID == Settings.SynthesizerVoiceID),
                Text = text,
            }
            , output
            , cancellationToken);
        }

        public async Task Play(Stream stream, AudioCodecType codec, Action<float>? updateProgressCallback = null)
        {
            Logger.Debug("Playing");
            updateProgressCallback?.Invoke(0);
            switch (codec)
            {
                case AudioCodecType.MP3:
                    await AudioPlayer.PlayMP3(stream, updateProgressCallback);
                    break;
                case AudioCodecType.WAV:
                    await AudioPlayer.PlayWAV(stream, updateProgressCallback);
                    break;
                default:
                    throw new InvalidOperationException("Invalid codec");
            }
            updateProgressCallback?.Invoke(1);
        }
    }

    public static class AudioManagerExt
    {
        public static void LoadFromSettings(this AudioManager manager)
        {
            manager.LoadSettings(new AudioManagerSettings
            {
                TranscribeKey = Settings.Default.AWSTranscribeKey,
                TranscribeSecretKey = Cryptography.Unprotect(Settings.Default.AWSTranscribeSecretKey),
                TranscribeRegion = Settings.Default.AWSTranscribeRegion,
                TranscribeLanguage = Settings.Default.AWSTranscribeLanguage,

                SynthesizerApiKey = Cryptography.Unprotect(Settings.Default.SynthesizerAzureKey),
                SynthesizerLanguageCode = Settings.Default.SynthesizerLanguageCode,
                SynthesizerVoiceID = Settings.Default.SynthesizerVoiceID,

                AudioBps = 16,
                AudioSampleRate = Settings.Default.AudioSampleRate,
                AudioChunkSize = Settings.Default.AudioChunkSize,
                AudioStereo = Settings.Default.AudioIsStereo,
            });
        }
    }
}
