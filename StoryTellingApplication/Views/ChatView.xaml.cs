﻿using ChatGPT.Models;
using StoryTellingApplication.Properties;
using StoryTellingApplication.ViewModels;
using System.Windows.Controls;

namespace StoryTellingApplication.Views
{
    public partial class ChatView : UserControl
    {
        public ChatViewModel Model { get; set; }

        public ChatView()
        {
            Model = new ChatViewModel();
            InitializeComponent();
        }

        private void InputTextox_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter && Settings.Default.AutoSend)
            {
                Model.SendCommand.Execute(sender);
                e.Handled = true;
            }
        }
    }
}
