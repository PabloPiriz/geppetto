﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StoryTellingApplication.ViewModels;

namespace StoryTellingApplication.Views
{
    /// <summary>
    /// Interaction logic for OptionsView.xaml
    /// </summary>
    public partial class OptionsView : Window
    {
        private ICommand? _SaveCommand = null;
        public ICommand SaveCommand => _SaveCommand ??= new RelayCommand((p) => Save());

        public OptionsView(OptionsViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        public void Save()
        {
            var model = DataContext as OptionsViewModel;
            Debug.Assert(model != null);

            model.AWSTranscribeSecretKey = AWSTranscribeSecretKey.Password;
            model.SynthesizerAzureKey = SynthesizerAzureKey.Password;
            model.OpenAIKey = OpenAIKey.Password;

            DialogResult = true;
        }
    }
}
