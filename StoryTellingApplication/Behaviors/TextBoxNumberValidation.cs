﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;

namespace StoryTellingApplication.Behaviors
{
    internal class TextBoxNumberValidation
    {
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = e.Text.All(c => c == '-' || c == '+' || (c >= '0' && c <= '9'));
        }
    }
}
