﻿using AudioSynthesizer;
using ChatGPT;
using ChatGPT.Models;
using Microsoft.CognitiveServices.Speech;
using Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using StoryTellingApplication;
using Amazon.Polly;
using StoryTellingApplication.Properties;

namespace StoryTellingApplication
{
    internal class Engine
    {

        private static Engine? _instance;
        public static Engine Instance => _instance ??= new Engine();

        public AudioManager AudioManager { get; private set; }

        private Engine() 
        {
            AudioManager = new AudioManager();
            AudioManager.LoadFromSettings();
        }

        public async Task<ChatItem> ProcessGPT(Chat chat)
        {
            using var client = new HttpClient();
            var GPTClient = new ChatGPTClient(client, Cryptography.Unprotect(Settings.Default.OpenAIKey));
            var result = await GPTClient.Chat(chat);
            return result?.Choices?.FirstOrDefault()?.Message ?? throw new InvalidDataException();
        }
    }
}
