﻿using System;
using System.Windows.Data;

namespace StoryTellingApplication.Converters
{
    internal class EnumToStringConverter : IValueConverter
    {
        public object? Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value.GetType() is Type)
            {
                return value.ToString();
            }
            throw new ArgumentException($"{value} is not a valid enum value");
        }

        public object? ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Enum? result = null;
            if (Enum.IsDefined(targetType, value))
            {
                result = (Enum?)Enum.Parse(targetType, value?.ToString() ?? "");
            }
            return result ?? throw new ArgumentException($"{value} is not a valid {targetType.FullName} value");
        }
    }


}
