﻿using Amazon.Runtime;
using Amazon.TranscribeStreamingService;
using AudioPlugins;
using AudioSynthesizer;
using AudioSynthesizer.Models;
using ChatGPT;
using ChatGPT.Models;
using Microsoft.CognitiveServices.Speech;
using NAudio.Wave;
using Shared;
using System.Buffers;
using System.Diagnostics;
using System.Text;
using System.Text.Json;
using static StoryTelling.VoiceChatManager.IVoiceChatSettings;

namespace StoryTelling
{
    public class VoiceChatManager
    {
        public enum StateType
        {
            Initializing,
            Listening,
            Performing,
            Synthesizing,
            Playing,
            Stopping,
            Idle,
        }
        public interface IVoiceChatSettings
        {
            public enum SynthesizeOutputCodecType
            {
                MP3,
                WAV,
                PCM,
            }
            // Global
            string SaveDirectoryPath { get; }
            string LanguageCode { get; }

            // Transcribe settings
            string TranscribeKey { get; }
            string TranscribeSecretKey { get; }
            string TranscribeRegion { get; }
            string TranscribeLanguage { get; }

            // Audio settings
            int AudioSampleRate { get; }
            int AudioBps { get; }
            bool AudioStereo { get; }
            int AudioChunkSizeMS { get; }

            // GPT
            string GPTApiKey { get; }
            string GPTModel { get; }

            // Synthesize
            string VoiceID { get; }
            SynthesizeOutputCodecType SynthesizeCodec { get; }
        }

        public delegate void OnStateChanged(StateType newState);
        public event OnStateChanged? OnStateChangedEvent;
        private StateType _state;
        public StateType State 
        { 
            get => _state; 
            private set
            {
                _state = value;
                OnStateChangedEvent?.Invoke(value);
            }
        }
        private bool nextStep { get; set; }
        public IVoiceChatSettings Settings { get; }

        private MicrophoneCapture? AudioCapture;
        private AmazonTranscribeStreamingClient? TranscribeClient;
        private ChatGPTClient? GPTClient;
        private ISynthesizerClient AudioSynthesizer;
        private MemoryStream SynthesizedData;
        private Chat Chat = new Chat() { Messages = new List<ChatItem>() };

        public VoiceChatManager(IVoiceChatSettings settings, ISynthesizerClient audioSynthesizer)
        {
            Settings = settings;
            SynthesizedData = new MemoryStream();
            AudioSynthesizer = audioSynthesizer;
            State = StateType.Initializing;
        }

        public async Task InitTranscribe()
        {
            Logger.Info($"Starting transcribe system: language {Settings.TranscribeLanguage} region {Settings.TranscribeRegion} chunkSizeMS {Settings.AudioChunkSizeMS}");
            Config config = new Config("pcm", $"{Settings.AudioSampleRate}", Settings.TranscribeLanguage)
            {
                EnableChannelIdentification = Settings.AudioStereo ? "true" : null,
                NumberOfChannels = Settings.AudioStereo ? "2" : null,
            };
            TranscribeClient = new AmazonTranscribeStreamingClient(Settings.TranscribeRegion, config, new BasicAWSCredentials(Settings.TranscribeKey, Settings.TranscribeSecretKey));
            TranscribeClient.TranscriptEvent += OnTranscribeEvent;
            TranscribeClient.TranscriptException += OnTranscribeException;
            Transcription = new Transcription();
            await Update();
        }

        public async Task InitAudioCapture()
        {
            Logger.Info($"Starting audio system: rate {Settings.AudioSampleRate} bps {Settings.AudioBps} stereo {Settings.AudioStereo}");
            var fmt = new WaveFormat(Settings.AudioSampleRate, Settings.AudioBps, Settings.AudioStereo ? 2 : 1);
            AudioCapture = new MicrophoneCapture(fmt);
            AudioCapture.StartCapture(autoPlay: false);
            await Update();
        }

        public async Task InitGPT()
        {
            GPTClient = new ChatGPTClient(new HttpClient(), Settings.GPTApiKey);
            Chat.Model = Settings.GPTModel;
            await Update();
        }

        private bool IsAudioSynthesizerInitialized = false;
        public async Task InitSynthesize()
        {
            await AudioSynthesizer.Init();
            IsAudioSynthesizerInitialized = true;
            await Update();
        }

        private async Task Listening()
        {
            if (State != StateType.Listening)
                throw new InvalidOperationException($"Invalid current state {State}");

            // Reset buffer
            Transcription.Reset();

            var cancellationTokenSource = new CancellationTokenSource();
            Task listen = Task.Run(async () =>
            {
                var cancellationToken = cancellationTokenSource.Token;
                await TranscribeClient.StartStreaming();
                AudioCapture.Resume();
                var reader = AudioCapture.GetAudioPipeReader();
                int chunkSize = Settings.AudioSampleRate * (Settings.AudioStereo ? 2 : 1) / (1000 / Settings.AudioChunkSizeMS);
                try
                {
                    var chunk = new byte[chunkSize];
                    while (State == StateType.Listening)
                    {
                        Array.Clear(chunk, 0, chunkSize);
                        var result = await reader.ReadAtLeastAsync(chunkSize, cancellationToken);
                        Debug.Assert(result.Buffer.Length >= chunkSize);

                        var bufferSequence = result.Buffer;
                        bufferSequence.Slice(0, chunkSize).CopyTo(new Span<byte>(chunk, 0, chunkSize));
                        reader.AdvanceTo(bufferSequence.GetPosition(chunkSize));

                        TranscribeClient.StreamBuffer(chunk);
                    }
                }
                catch (TaskCanceledException)
                {
                }
            });

            while (true)
            {
                var readTask = Util.ReadLineAsync();
                var finished = await Task.WhenAny(listen, readTask);

                if (finished == listen)
                {
                    State = StateType.Stopping;
                    break;
                }
                var input = readTask.Result;
                if (input == "")
                {
                    cancellationTokenSource.Cancel();
                    Chat.Messages.Add(new ChatItem { Role = ChatRoleType.user.ToString(), Content = Transcription.StaticTranscription });
                    nextStep = true;
                    break;
                }
                else if (string.Equals(input, "reset", StringComparison.InvariantCultureIgnoreCase))
                {
                    Logger.Info("Listening: Reset");
                    Transcription.Reset();
                    continue;
                }
                else if (string.Equals(input, "save", StringComparison.InvariantCultureIgnoreCase))
                {
                    var guid = Guid.NewGuid();
                    Logger.Info($"* Saveing Chat '{guid}'");
                    var file = Path.Combine(Settings.SaveDirectoryPath, $"save_{guid}.json");
                    using var fileStream = new FileStream(file, FileMode.CreateNew, FileAccess.Write, FileShare.Read);
                    await JsonSerializer.SerializeAsync(fileStream, Chat, new JsonSerializerOptions { WriteIndented = true });
                }
                else if (input.StartsWith("load ", StringComparison.InvariantCultureIgnoreCase))
                {
                    Guid guid;
                    if (Guid.TryParse(input.Substring("load ".Length), out guid))
                    {
                        var file = Path.Combine(Settings.SaveDirectoryPath, $"save_{guid}.json");
                        if (!File.Exists(file)) continue;
                        using var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
                        var newChat = await JsonSerializer.DeserializeAsync<Chat>(fileStream);
                        if (newChat != null)
                        {
                            Transcription.Reset();
                            Chat = newChat;
                            Logger.Debug($"Loaded: {string.Join("\n", Chat.Messages.Select(m => $"{m.Role}: {m.Content}"))}");
                            continue;
                        }
                    }
                }
                else
                {
                    var dynamicData = "";
                    if (Transcription.DynamicBlock.Length > 0)
                        dynamicData = Transcription.DynamicBlock + " ";
                    dynamicData += input;
                    Transcription.SetDynamic(dynamicData);
                    Transcription.FlushDynamic();
                    Logger.Info($"Listener: {Transcription.FullTranscription}");
                }
            }
            AudioCapture.Pause();
            await TranscribeClient.StopStreaming();
            await Update();
        }

        Transcription Transcription = new Transcription();
        private void OnTranscribeEvent(object? sender, TranscriptEvent transcriptEvent)
        {
            var transcription = transcriptEvent.Transcript?.Results?.FirstOrDefault();
            var message = transcription?.Alternatives?.FirstOrDefault()?.Transcript;
            if (transcription == null || message == null)
                return;

            if (Transcription.FullTranscription.Length == 0)
            {
                Logger.Debug("Listening receiving transcription");
            }

            if (transcription.IsPartial)
            {
                Transcription.SetDynamic(message);
                // Logger.Debug($"Listener: {Transcription.FullTranscription}");
            }
            else
            {
                Transcription.SetDynamic(message);
                Transcription.FlushDynamic();
                Logger.Info($"Listener: {Transcription.FullTranscription}");
            }
        }

        private void OnTranscribeException(object? sender, TranscribeException transcriptException)
        {
            Logger.Error(transcriptException?.ToString() ?? nameof(TranscribeException));
        }

        // For debugging
        private int NewMessageIndex = 0;
        private async Task SendToGPT()
        {
            if (State != StateType.Performing)
                throw new InvalidOperationException($"Invalid current state {State}");

            Logger.Debug("============================================");
            Logger.Debug("Sending to GPT");
            Chat.Messages.ForEach(m => Logger.Debug($"{m.Role}: {m.Content}"));
            Logger.Debug("============================================");
            // var newMessages = Chat.Messages.Skip(NewMessageIndex).Select(m => $"ToChat: {m.Content}");
            // Logger.Debug(string.Join("\n", newMessages));

            NewMessageIndex = Chat.Messages.Count;
            var result = await GPTClient.Chat(Chat);
            Logger.Info($"FromChat: {result.Choices.Single().Message.Content}");
            Chat.Messages.Add(result.Choices.Single().Message);
            nextStep = true;
            await Update();
        }

        private async Task Synthesize()
        {
            if (State != StateType.Synthesizing)
                throw new InvalidOperationException($"Invalid current state {State}");

            SynthesizedData = new MemoryStream();
            var message = Chat.Messages.Last().Content;
            await AudioSynthesizer.Synthesize(new SynthesizeJob
            {
                Language = Settings.LanguageCode,
                Voice = (await AudioSynthesizer.GetVoices()).Single(v => v.ID == Settings.VoiceID),
                Text = message,
            }, SynthesizedData);
            SynthesizedData.Position = 0;

            nextStep = true;
            await Update();
        }

        private async Task Play(Action<float>? updateProgressCallback = null)
        {
            if (State != StateType.Playing)
                throw new InvalidOperationException($"Invalid current state {State}");
            Logger.Debug("Playing");
            updateProgressCallback?.Invoke(0);
            switch (Settings.SynthesizeCodec)
            {
                case SynthesizeOutputCodecType.MP3:
                    await AudioPlayer.PlayMP3(SynthesizedData, updateProgressCallback);
                    break;
                case SynthesizeOutputCodecType.WAV:
                    await AudioPlayer.PlayWAV(SynthesizedData, updateProgressCallback);
                    break;
                default:
                    throw new InvalidOperationException("Invalid codec");
            }
            updateProgressCallback?.Invoke(1);
            nextStep = true;
            await Update();
        }

        private async Task Update()
        {
            while (true)
            {
                if (State == StateType.Stopping)
                {
                    break; // TODO: resolve
                }
                if (State == StateType.Initializing && !nextStep)
                {
                    if (TranscribeClient != null && AudioCapture != null && GPTClient != null && IsAudioSynthesizerInitialized)
                    {
                        nextStep = true;
                        continue;
                    }
                    break;
                }
                if (nextStep)
                {
                    if (State == StateType.Initializing)
                    {
                        State = StateType.Idle;
                        continue;
                    }
                    else if (State == StateType.Idle)
                    {
                        State = StateType.Listening;
                        _ = Listening();
                    }
                    else if (State == StateType.Listening)
                    {
                        State = StateType.Performing;
                        _ = SendToGPT();
                    }
                    else if (State == StateType.Performing)
                    {
                        State = StateType.Synthesizing;
                        _ = Synthesize();
                    }
                    else if (State == StateType.Synthesizing)
                    {
                        State = StateType.Playing;
                        _ = Play();
                    }
                    else if (State == StateType.Playing)
                    {
                        State = StateType.Idle;
                        continue;
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }
                    break;
                }
            }
            await Task.CompletedTask;
        }
    }

#nullable enable

    public class VoiceChatSettings : VoiceChatManager.IVoiceChatSettings
    {
        public string SaveDirectoryPath => "c:/test/gpt/";
        public string LanguageCode => "es-ES";

        public string TranscribeKey => "";
        public string TranscribeSecretKey => Cryptography.Unprotect("");
        public string TranscribeRegion => "us-east-1";
        public string TranscribeLanguage => "es-US";

        public int AudioSampleRate => 16000;
        public int AudioBps => 16;
        public bool AudioStereo => false;
        public int AudioChunkSizeMS => 100;

        public string GPTApiKey => Cryptography.Unprotect("");
        public string GPTModel => Chat.MODEL_V35;

        public string VoiceID { get; set; } = null!;
        public SynthesizeOutputCodecType SynthesizeCodec { get; set; }
    }


    public class Transcription
    {
        public StringBuilder StaticBlock { get; } = new StringBuilder();
        private string _dynamicBlock { get; set; } = "";
        public string DynamicBlock { get => _dynamicBlock; }
        public delegate void TranscriptionEvent(Transcription transcription);
        public event TranscriptionEvent TranscriptionChanged = null!;

        public void FlushDynamic()
        {
            lock (this)
            {
                if (StaticBlock.Length > 0)
                {
                    StaticBlock.Append(' ');
                }
                StaticBlock.Append(DynamicBlock);
                _dynamicBlock = "";
                TranscriptionChanged?.Invoke(this);
            }
        }
        public void SetDynamic(string text)
        {
            lock (this)
            {
                _dynamicBlock = text;
                TranscriptionChanged?.Invoke(this);
            }
        }
        public void Reset(bool preserveDynamic = false)
        {
            lock (this)
            {
                StaticBlock.Clear();
                if (!preserveDynamic)
                {
                    _dynamicBlock = "";
                }
            }
        }
        public string StaticTranscription => StaticBlock.ToString();
        private string DynamicTranscriptionDisplay => DynamicBlock.Length > 0 ? $"{DynamicBlock}*" : "";
        public string FullTranscription => StaticTranscription + (DynamicBlock.Length > 0 ? "\n" : "") + DynamicTranscriptionDisplay;
    }
}
