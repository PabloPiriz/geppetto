﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioSynthesizer.Models
{
    public class SynthesizeVoice
    {
        public string ID { get; set; } = null!;
        public string Name { get; set; } = null!;
        public string LanguageCode { get; set; } = null!;
        public string? Gender { get; set; }
        public object? ExtraDetails { get; set; }
    }

    public class SynthesizeJob
    {
        public SynthesizeVoice Voice { get; set; } = null!;
        public string Text { get; set; } = null!;
        public string TextFormat { get; set; } = null!;
        public string OutputFormat { get; set; } = "mp3";
        public string? Language { get; set; }
    }
    public class SynthesizeException : Exception
    {
        public SynthesizeException(string msg) : base(msg) { }
    }

    public abstract class ISynthesizerClient
    {
        public abstract Task Init();
        public abstract Task<List<SynthesizeVoice>> GetVoices(string? languageCode = null, CancellationToken cancellationToken = default);
        public abstract Task Synthesize(SynthesizeJob job, Stream destination, CancellationToken cancellationToken = default);
    }
}
