﻿using Amazon;
using Amazon.Polly.Model;
using Amazon.Polly;
using Amazon.Runtime;
using Amazon.S3.Util;
using Amazon.S3;
using AudioSynthesizer.Models;
using Shared.AwsS3;

namespace AudioSynthesizer
{

    public class AmazonSynthesizerClient : ISynthesizerClient
    {
        public class AmazonSynthesizerSettings
        {
            public string Region { get; set; } = null!;
            public AWSCredentials Credentials { get; set; } = null!;
            public AWSS3Location OutputLocation { get; set; }
            public TimeSpan ResultCheckInterval { get; set; } = TimeSpan.FromMilliseconds(500);
        }
        public class VoiceExtraDetails
        {
            public List<string> SupportedEngines { get; set; } = new List<string>();
            public List<string> AdditionalSupportedLanguageCodes { get; set; } = new List<string>();
        }

        private AmazonSynthesizerSettings? Settings;

        public AmazonSynthesizerClient(AmazonSynthesizerSettings settings)
        {
            this.Settings = settings;
        }
        public override async Task Init() => await Task.CompletedTask;

        private AmazonPollyClient GetClient()
        {
            if (Settings == null)
                throw new InvalidOperationException("Not initialized");

            return new AmazonPollyClient(Settings.Credentials,
                new AmazonPollyConfig
                {
                    RegionEndpoint = RegionEndpoint.GetBySystemName(Settings.Region),
                }
            );
        }

        private List<SynthesizeVoice>? Voices = null;
        public override async Task<List<SynthesizeVoice>> GetVoices(string? languageCode = null, CancellationToken cancellationToken = default)
            => Voices ??= await GetVoicesImpl(languageCode, cancellationToken);
        private async Task<List<SynthesizeVoice>> GetVoicesImpl(string? languageCode = null, CancellationToken cancellationToken = default)
        {
            using var client = GetClient();
            var req = new DescribeVoicesRequest();
            if (languageCode != null) req.LanguageCode = languageCode;

            var resp = await client.DescribeVoicesAsync(req, cancellationToken);
            return resp.Voices
                .Select(v => new SynthesizeVoice
                {
                    ID = v.Id,
                    Name = v.Name,
                    LanguageCode = v.LanguageCode,
                    Gender = v.Gender,
                    ExtraDetails = new VoiceExtraDetails
                    {
                        SupportedEngines = v.SupportedEngines,
                        AdditionalSupportedLanguageCodes = v.AdditionalLanguageCodes,
                    }
                }).ToList();
        }

        private StartSpeechSynthesisTaskRequest BuildSynthesizeRequest(SynthesizeJob job)
        {
            if (Settings == null)
                throw new InvalidOperationException("Not initialized");

            var extraVoiceDetails = job.Voice.ExtraDetails as VoiceExtraDetails;
            return new StartSpeechSynthesisTaskRequest
            {
                OutputFormat = job.OutputFormat,
                Text = job.Text,
                VoiceId = job.Voice.ID,
                OutputS3BucketName = Settings.OutputLocation.Bucket,
                OutputS3KeyPrefix = Settings.OutputLocation.Key,
                Engine = extraVoiceDetails?.SupportedEngines?.Any(e => e == "neural") ?? false ? "neural" : "standard",
            };
        }

        public override async Task Synthesize(SynthesizeJob job, Stream destination, CancellationToken cancellationToken = default)
        {
            if (Settings == null)
                throw new InvalidOperationException("Not initialized");

            using var client = GetClient();

            var awsTaskRequest = BuildSynthesizeRequest(job);
            if (job.TextFormat != null) awsTaskRequest.TextType = job.TextFormat;
            if (job.Language != null) awsTaskRequest.LanguageCode = job.Language;


            var awsTask = await client.StartSpeechSynthesisTaskAsync(awsTaskRequest, cancellationToken);
            while (true)
            {
                var awsTaskUpdate = await client.GetSpeechSynthesisTaskAsync(new GetSpeechSynthesisTaskRequest()
                {
                    TaskId = awsTask.SynthesisTask.TaskId
                }, cancellationToken);

                if (awsTaskUpdate.SynthesisTask.TaskStatus == Amazon.Polly.TaskStatus.Failed)
                {
                    throw new SynthesizeException(awsTaskUpdate.SynthesisTask.TaskStatusReason);
                }
                if (awsTaskUpdate.SynthesisTask.TaskStatus == Amazon.Polly.TaskStatus.Completed)
                {
                    await DownloadS3File(awsTaskUpdate.SynthesisTask.OutputUri, destination, cancellationToken);
                    _ = DeleteS3File(awsTaskUpdate.SynthesisTask.OutputUri, default);
                    break;
                }
                await Task.Delay(Settings.ResultCheckInterval, cancellationToken);
            }
        }

        private async Task DownloadS3File(string url, Stream stream, CancellationToken cancellationToken)
        {
            if (Settings == null)
                throw new InvalidOperationException("Not initialized");

            AmazonS3Uri awsUri;
            if (!AmazonS3Uri.TryParseAmazonS3Uri(url, out awsUri))
            {
                throw new ArgumentException();
            }

            using var client = new AmazonS3Client(Settings.Credentials, RegionEndpoint.GetBySystemName(Settings.Region));
            var awsObject = await client.GetObjectAsync(awsUri.Bucket, awsUri.Key, cancellationToken);
            if (awsObject == null)
            {
                throw new FileNotFoundException();
            }
            await awsObject.ResponseStream.CopyToAsync(stream);
        }
        private async Task DeleteS3File(string url, CancellationToken cancellationToken)
        {
            if (Settings == null)
                throw new InvalidOperationException("Not initialized");

            AmazonS3Uri awsUri;
            if (!AmazonS3Uri.TryParseAmazonS3Uri(url, out awsUri))
            {
                throw new ArgumentException();
            }

            using var client = new AmazonS3Client(Settings.Credentials, RegionEndpoint.GetBySystemName(Settings.Region));
            await client.DeleteObjectAsync(awsUri.Bucket, awsUri.Key, cancellationToken);
        }
    }
}
