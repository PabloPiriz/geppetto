﻿using AudioSynthesizer.Models;
using Microsoft.CognitiveServices.Speech.Audio;
using Microsoft.CognitiveServices.Speech;

namespace AudioSynthesizer
{
    public class AzureSynthesizerClient : ISynthesizerClient, IDisposable
    {
        public enum InputFormatType
        {
            SSML,
            Text
        }

        private string Key { get; set; }
        private string Region { get; set; }
        private InputFormatType InputFormat { get; set; }
        private SpeechSynthesisOutputFormat OutputFormat { get; set; }

        public AzureSynthesizerClient(string key, string region, InputFormatType inputFormat, SpeechSynthesisOutputFormat outputFormat)
        {
            Key = key;
            Region = region;
            InputFormat = inputFormat;
            OutputFormat = outputFormat;
        }

        public override async Task Init()
        {
            await Task.CompletedTask;
        }
        private List<SynthesizeVoice>? Voices = null;
        public override async Task<List<SynthesizeVoice>> GetVoices(string? languageCode = null, CancellationToken cancellationToken = default)
            => Voices ??= await GetVoicesImpl(languageCode, cancellationToken);
        private async Task<List<SynthesizeVoice>> GetVoicesImpl(string? languageCode = null, CancellationToken cancellationToken = default)
        {
            using var synthesizer = new SpeechSynthesizer(SpeechConfig.FromSubscription(Key, Region));
            var resp = await synthesizer.GetVoicesAsync();
            return resp.Voices
                .Select(v => new SynthesizeVoice
                {
                    ID = v.ShortName,
                    Name = v.ShortName,
                    Gender = v.Gender.ToString(),
                    LanguageCode = v.Locale,
                    ExtraDetails = v,
                    // ExtraDetails = v,
                })
                .ToList();
        }

        private class PushAudioOutputStreamWrapper : PushAudioOutputStreamCallback
        {
            private Stream Stream;
            private bool KeepOpen = false;
            private bool Disposed = false;

            public PushAudioOutputStreamWrapper(Stream stream, bool keepOpen)
            {
                Stream = stream;
            }
            public override uint Write(byte[] dataBuffer)
            {
                Stream.Write(dataBuffer, 0, dataBuffer.Length);
                return (uint)dataBuffer.Length;
            }
            public new void Dispose()
            {
                if (Disposed)
                    return;

                Disposed = true;
                if (!KeepOpen)
                {
                    Stream?.Dispose();
                }
            }
        }
        public override async Task Synthesize(SynthesizeJob job, Stream destination, CancellationToken cancellationToken = default)
        {
            SpeechConfig config = SpeechConfig.FromSubscription(Key, Region);
            config.SetSpeechSynthesisOutputFormat(OutputFormat);
            config.SpeechSynthesisVoiceName = job.Voice.ID;
            var audioOutputStream = AudioOutputStream.CreatePushStream(new PushAudioOutputStreamWrapper(destination, keepOpen: true));
            using var synthesizer = new SpeechSynthesizer(config, AudioConfig.FromStreamOutput(audioOutputStream));
            SpeechSynthesisResult response;
            switch (InputFormat)
            {
                case InputFormatType.SSML:
                    response = await synthesizer.SpeakSsmlAsync(job.Text);
                    break;
                case InputFormatType.Text:
                    response = await synthesizer.SpeakTextAsync(job.Text);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        private bool Disposed = false;
        public void Dispose()
        {
            if (Disposed) return;

            Disposed = true;
        }
    }

}
