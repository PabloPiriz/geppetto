﻿// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

#nullable enable

using Amazon.Runtime;
using Amazon.Runtime.EventStreams;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using Websocket.Client;

namespace Amazon.TranscribeStreamingService
{

    public class AmazonTranscribeStreamingClient
    {

        public event EventHandler<TranscriptEvent>? TranscriptEvent;
        public event EventHandler<TranscribeException>? TranscriptException;


        private readonly string _region;
        private readonly Config _config;
        private readonly AWSCredentials _credentials;
        private WebsocketClient? _client;

        public bool IsConnected
        {
            get
            {
                if (_client != null)
                {
                    return _client.IsRunning;
                }
                else return false;
            }
        }

        /// <summary>
        /// Instantiate a new streaming client.
        /// </summary>
        /// <param name="region">Which region to connect to, for example, us-west-2</param>
        /// <param name="config">Configuration parameters for the streaming session</param>
        /// <param name="credentials">AWS Credentials</param>
        public AmazonTranscribeStreamingClient(string region, Config config, AWSCredentials? credentials = null)
        {
            this._region = region;
            this._config = config;
            if (credentials == null) this._credentials = FallbackCredentialsFactory.GetCredentials();
            else this._credentials = credentials;
        }

        /// <summary>
        /// This opens a new websocket session and returns once it is connected.
        /// </summary>
        /// <returns>Nothing.</returns>
        public async Task StartStreaming()
        {
            TranscribePresignedUrl presigned = new TranscribePresignedUrl(this._region, this._config, this._credentials);
            string webSocketUrl = presigned.GetPresignedUrl();
            Debug.WriteLine(webSocketUrl);
            var url = new Uri(webSocketUrl);

            _client = new WebsocketClient(url);

            _client.ReconnectTimeout = TimeSpan.FromSeconds(30);
            _client.ReconnectionHappened.Subscribe(info =>
                Debug.WriteLine($"Reconnection happened, type: {info.Type}"));

            _client.DisconnectionHappened.Subscribe(info =>
                Debug.WriteLine($"Disconnect happened, type: {info.Type}, {info.Exception}"));

            _client.MessageReceived.Subscribe(msg =>
            {
                EventStreamMessage message = EventStreamMessage.FromBuffer(msg.Binary, 0, msg.Binary.Length);
                if (message.Headers.ContainsKey(":exception-type"))
                {
                    TranscribeException exception = new TranscribeException()
                    {
                        ExceptionType = message.Headers[":exception-type"].AsString(),
                        Message = System.Text.Encoding.UTF8.GetString(message.Payload)
                    };
                    this.TranscriptException?.Invoke(this, exception);
                }
                else if (message.Headers.ContainsKey(":event-type"))
                {
                    string json = System.Text.Encoding.UTF8.GetString(message.Payload);
                    // Console.WriteLine(json);
                    TranscriptEvent? transcriptEvent = JsonSerializer.Deserialize<TranscriptEvent>(json);
                    if (transcriptEvent != null) this.TranscriptEvent?.Invoke(this, transcriptEvent);
                }
            });
            await _client.Start();
        }

        /// <summary>
        /// If the websocket is open, this will terminate the connection.
        /// </summary>
        /// <returns>Nothing.</returns>
        public async Task StopStreaming()
        {
            if (this._client != null)
            {
                await _client.Stop(System.Net.WebSockets.WebSocketCloseStatus.NormalClosure, "Closing");
            }
        }

        /// <summary>
        /// This is a helper function designed to open an audio file and stream it, as if it was real
        /// time, to Amazon Transcribe.  This will help you consistently test streaming.
        /// </summary>
        /// <param name="filename">A path to the audio file to stream.</param>
        /// <param name="chunkSize">The size, in bytes, of the chunks to send. We suggest 100-200ms chunks.</param>
        /// <param name="sleepTimeMs">The amount of time to slepe between chunks to simulate real-time. 
        /// Transcribe Streaming will queue a few seconds of audio if you send ahead, but will error if the forward
        /// buffer it too large.
        /// </param>
        /// <exception cref="Exception">This will throw a generic exception if the client is not connected.</exception>
        public void StreamFile(string filename, int chunkSize, int sleepTimeMs)
        {
            if (this._client?.IsRunning == true)
            {
                List<byte[]> chunks = StaticAudioFile.GetAudioStreamChunks(filename, chunkSize);
                int currentChunk = 0;

                while (currentChunk < chunks.Count)
                {
                    AudioEvent audioEvent = new AudioEvent(chunks[currentChunk]);
                    byte[] buffer = audioEvent.Serialize();
                    currentChunk += 1;
                    // Debug.WriteLine("Sending " + currentChunk);
                    _client.Send(buffer);
                    Thread.Sleep(sleepTimeMs);
                }
            }
            else
            {
                throw new Exception("Client not connected.");
            }
        }

        /// <summary>
        /// This will stream a byte array 'chunk' to Transcribe. This is usually 100-200ms worth of audio.
        /// </summary>
        /// <param name="buffer">Byte array of the audio data chunk to send</param>
        /// <exception cref="Exception">This will throw an exception if the client is not connected.</exception>
        public void StreamBuffer(byte[] buffer)
        {
            if (this._client?.IsRunning == true)
            {
                AudioEvent audioEvent = new AudioEvent(buffer);
                byte[] eventBuffer = audioEvent.Serialize();
                _client.Send(eventBuffer);
            }
            else
            {
                throw new Exception("Client not connected.");
            }
        }
    }

    #region Util
    public interface ByteSerializable
    {
        public byte[] Serialize();
    }
    public class Utils
    {

        public static ushort SwapBytes(ushort x)
        {
            return (ushort)((ushort)((x & 0xff) << 8) | ((x >> 8) & 0xff));
        }

        public static uint SwapBytes(uint x)
        {
            return ((x & 0x000000ff) << 24) +
                ((x & 0x0000ff00) << 8) +
                ((x & 0x00ff0000) >> 8) +
                ((x & 0xff000000) >> 24);
        }

        public static ulong SwapBytes(ulong value)
        {
            ulong uvalue = value;
            ulong swapped =
                ((0x00000000000000FF) & (uvalue >> 56)
                | (0x000000000000FF00) & (uvalue >> 40)
                | (0x0000000000FF0000) & (uvalue >> 24)
                | (0x00000000FF000000) & (uvalue >> 8)
                | (0x000000FF00000000) & (uvalue << 8)
                | (0x0000FF0000000000) & (uvalue << 24)
                | (0x00FF000000000000) & (uvalue << 40)
                | (0xFF00000000000000) & (uvalue << 56));
            return swapped;
        }
    }
    public static class DictionaryExtensions
    {
        public static void Update<K, V>(this IDictionary<K, V> me, IDictionary<K, V> other)
        {
            foreach (var x in other)
            {
                me[x.Key] = x.Value;
            }
        }
    }
    /// <summary>
    /// Performs 32-bit reversed cyclic redundancy checks.
    /// </summary>
    public class Crc32
    {
        private static Crc32? _singleton;
        public static Crc32 Singleton
        {
            get
            {
                if (_singleton == null) _singleton = new Crc32();
                return _singleton;
            }
        }

        #region Constants
        /// <summary>
        /// Generator polynomial (modulo 2) for the reversed CRC32 algorithm. 
        /// </summary>
        private const UInt32 s_generator = 0xEDB88320;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of the Crc32 class.
        /// </summary>
        public Crc32()
        {
            // Constructs the checksum lookup table. Used to optimize the checksum.
            m_checksumTable = Enumerable.Range(0, 256).Select(i =>
            {
                var tableEntry = (uint)i;
                for (var j = 0; j < 8; ++j)
                {
                    tableEntry = ((tableEntry & 1) != 0)
                        ? (s_generator ^ (tableEntry >> 1))
                        : (tableEntry >> 1);
                }
                return tableEntry;
            }).ToArray();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Calculates the checksum of the byte stream.
        /// </summary>
        /// <param name="byteStream">The byte stream to calculate the checksum for.</param>
        /// <returns>A 32-bit reversed checksum.</returns>
        public UInt32 Get<T>(IEnumerable<T> byteStream)
        {
            try
            {
                // Initialize checksumRegister to 0xFFFFFFFF and calculate the checksum.
                return ~byteStream.Aggregate(0xFFFFFFFF, (checksumRegister, currentByte) =>
                        (m_checksumTable[(checksumRegister & 0xFF) ^ Convert.ToByte(currentByte)] ^ (checksumRegister >> 8)));
            }
            catch (FormatException e)
            {
                throw new Exception("Could not read the stream out as bytes.", e);
            }
            catch (InvalidCastException e)
            {
                throw new Exception("Could not read the stream out as bytes.", e);
            }
            catch (OverflowException e)
            {
                throw new Exception("Could not read the stream out as bytes.", e);
            }
        }
        #endregion

        #region Fields
        /// <summary>
        /// Contains a cache of calculated checksum chunks.
        /// </summary>
        private readonly UInt32[] m_checksumTable;

        #endregion
    }
    #endregion //Util


    #region Models

    public class AudioEvent : ByteSerializable
    {
        private byte[] _audioData;

        public AudioEvent(byte[] audioData)
        {
            this._audioData = audioData;
        }

        public byte[] Serialize()
        {
            Message message = new Message();
            message.Payload = this._audioData;
            message.Headers = new Header[3];
            message.Headers[0] = new Header(":content-type", "application/octet-stream");
            message.Headers[1] = new Header(":event-type", "AudioEvent");
            message.Headers[2] = new Header(":message-type", "event");
            message.CalculatePrelude();

            MessageCrc msgAndCrc = new MessageCrc(message);

            return msgAndCrc.Serialize();
        }
    }
    public class AudioEventMessage
    {
        public UInt32 totallength { get; set; }
        public UInt32 headerslength { get; set; }
        public UInt32 crc { get; set; }
        public Dictionary<string, Dictionary<string, string>>? headers { get; set; }
        public byte[]? body { get; set; }
        public UInt32 msgcrc { get; set; }
    }
    public class TranscriptEventArgs : EventArgs
    {
        public string? Transcript { get; set; }
    }
    public struct Header : ByteSerializable
    {
        public byte NameLength;
        public byte[] Name;
        public byte ValueType;
        public UInt16 ValueLength;
        public byte[] Value;

        public Header(string Name, string Value)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            this.Name = utf8.GetBytes(Name);
            this.Value = utf8.GetBytes(Value);
            this.ValueType = 7;
            this.NameLength = (byte)Name.Length;
            this.ValueLength = Convert.ToUInt16(Value.Length);
        }

        public UInt32 GetLength()
        {
            return Convert.ToUInt32(4 + Name.Length + Value.Length);
        }

        public byte[] Serialize()
        {
            byte[] arr = new byte[1 + Name.Length + 1 + 2 + Value.Length];
            int pos = 0;
            // copy name length
            System.Buffer.BlockCopy(BitConverter.GetBytes(NameLength), 0, arr, pos, 1);
            pos += 1;

            // copy name
            System.Buffer.BlockCopy(Name, 0, arr, pos, Name.Length);
            pos += Name.Length;

            // copy value type
            System.Buffer.BlockCopy(BitConverter.GetBytes(ValueType), 0, arr, pos, 1);
            pos += 1;

            // copy value length
            byte[] valueLength = BitConverter.GetBytes(ValueLength);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(valueLength);
            }

            System.Buffer.BlockCopy(valueLength, 0, arr, pos, 2);
            pos += 2;

            // copy value 
            System.Buffer.BlockCopy(Value, 0, arr, pos, Value.Length);
            pos += Value.Length;

            return arr;
        }

    }
    public struct Message : ByteSerializable
    {
        public Prelude Prelude;

        public UInt32 PreludeCrc;

        public Header[] Headers;

        public byte[] Payload;

        public void CalculatePrelude()
        {
            // calculate header length
            this.Prelude.HeadersByteLength = 0;
            foreach (var header in this.Headers)
            {
                this.Prelude.HeadersByteLength += header.GetLength();
            }
            this.Prelude.TotalByteLength = Convert.ToUInt32(12 + this.Prelude.HeadersByteLength + this.Payload.Length + 4);
            this.PreludeCrc = Crc32.Singleton.Get(this.Prelude.Serialize());
            if (BitConverter.IsLittleEndian)
            {
                this.PreludeCrc = Utils.SwapBytes(this.PreludeCrc);
            }
        }

        private byte[] SerializeHeaders()
        {
            int headerSize = 0;
            byte[][] serializedHeaders = new byte[Headers.Length][];
            for (int i = 0; i < Headers.Length; i++)
            {
                serializedHeaders[i] = Headers[i].Serialize();
                headerSize += serializedHeaders[i].Length;
            }

            byte[] arr = new byte[headerSize];
            int pos = 0;
            for (int i = 0; i < Headers.Length; i++)
            {
                System.Buffer.BlockCopy(serializedHeaders[i], 0, arr, pos, serializedHeaders[i].Length);
                pos += serializedHeaders[i].Length;
            }

            return arr;
        }

        public byte[] Serialize()
        {
            CalculatePrelude();
            byte[] prelude = Prelude.Serialize();
            byte[] preludeCrc = BitConverter.GetBytes(PreludeCrc);
            byte[] headers = this.SerializeHeaders();

            byte[] arr = new byte[prelude.Length + preludeCrc.Length + headers.Length + Payload.Length];
            int pos = 0;

            // copy prelude
            System.Buffer.BlockCopy(prelude, 0, arr, pos, prelude.Length);
            pos += prelude.Length;

            // copy preludecrc
            System.Buffer.BlockCopy(preludeCrc, 0, arr, pos, preludeCrc.Length);
            pos += preludeCrc.Length;

            // copy headers
            System.Buffer.BlockCopy(headers, 0, arr, pos, headers.Length);
            pos += headers.Length;

            // copy headers
            System.Buffer.BlockCopy(Payload, 0, arr, pos, Payload.Length);
            pos += Payload.Length;

            return arr;
        }
    }
    public struct MessageCrc : ByteSerializable
    {
        public Message Message;

        public UInt32 Crc;

        public MessageCrc(Message message)
        {
            this.Message = message;
            this.Message.CalculatePrelude();
            this.Crc = Crc32.Singleton.Get(message.Serialize());
        }

        public byte[] Serialize()
        {
            byte[] message = Message.Serialize();
            byte[] crc = BitConverter.GetBytes(Crc);

            byte[] arr = new byte[message.Length + crc.Length];
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(crc);
            }

            // copy message
            System.Buffer.BlockCopy(message, 0, arr, 0, message.Length);
            System.Buffer.BlockCopy(crc, 0, arr, message.Length, crc.Length);

            return arr;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Prelude : ByteSerializable
    {

        public UInt32 TotalByteLength;

        public UInt32 HeadersByteLength;

        public byte[] Serialize()
        {
            byte[] serialized = new byte[8];

            byte[] totalBytesLength = BitConverter.GetBytes(TotalByteLength);
            byte[] headerBytesLength = BitConverter.GetBytes(HeadersByteLength);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(totalBytesLength);
                Array.Reverse(headerBytesLength);
            }

            System.Buffer.BlockCopy(totalBytesLength, 0, serialized, 0, 4);
            System.Buffer.BlockCopy(headerBytesLength, 0, serialized, 4, 4);
            return serialized;
        }
    }
    public class StaticAudioFile
    {

        public static List<byte[]> GetAudioStreamChunks(string filename, int chunkSize)
        {
            List<byte[]> bytes = new List<byte[]>();
            // string filename = "sample2.wav";
            FileStream SourceStream = File.Open(filename, FileMode.Open);
            var br = new BinaryReader(SourceStream);
            while (br.BaseStream.Position != br.BaseStream.Length)
            {
                byte[] chunk = new byte[chunkSize];
                chunk = br.ReadBytes(chunkSize);
                bytes.Add(chunk);
            }
            return bytes;
        }
    }
    public class TranscribePresignedUrl
    {
        private const string Service = "transcribe";
        private const string Path = "/stream-transcription-websocket";
        private const string Scheme = "AWS4";
        private const string Algorithm = "HMAC-SHA256";
        private const string Terminator = "aws4_request";
        private const string HmacSha256 = "HMACSHA256";

        private readonly string _region;
        private readonly Config _config;
        private readonly AWSCredentials _credentials;

        public TranscribePresignedUrl(string region, Config config, AWSCredentials credentials)
        {
            _credentials = credentials;
            _region = region;
            _config = config;
            //_region =  Environment.GetEnvironmentVariable("REGION");
            //_awsAccessKey = Environment.GetEnvironmentVariable("AWS_ACCESS_KEY_ID");
            //_awsSecretKey = Environment.GetEnvironmentVariable("AWS_SECRET_ACCESS_KEY");
        }

        public string GetPresignedUrl()
        {
            return GenerateUrl();
        }

        private string GenerateUrl()
        {
            var host = $"transcribestreaming.{_region}.amazonaws.com:8443";
            var dateNow = DateTime.UtcNow;
            var dateString = dateNow.ToString("yyyyMMdd");
            var dateTimeString = dateNow.ToString("yyyyMMddTHHmmssZ");
            var credentialScope = $"{dateString}/{_region}/{Service}/{Terminator}";
            var query = GenerateQueryParams(dateTimeString, credentialScope);
            var signature = GetSignature(host, dateString, dateTimeString, credentialScope);
            return $"wss://{host}{Path}?{query}&X-Amz-Signature={signature}";
        }

        private string GenerateQueryParams(string dateTimeString, string credentialScope)
        {
            var credentials = $"{_credentials.GetCredentials().AccessKey}/{credentialScope}";
            var result = new Dictionary<string, string>
            {
                {"X-Amz-Algorithm", "AWS4-HMAC-SHA256"},
                {"X-Amz-Credential", credentials},
                {"X-Amz-Date", dateTimeString},
                {"X-Amz-Expires", "30"},
                {"X-Amz-SignedHeaders", "host"}
            };
            result.Update(_config.GetDictionary());
            return string.Join("&", result.Select(x => $"{x.Key}={Uri.EscapeDataString(x.Value)}"));
        }
        private string GetSignature(string host, string dateString, string dateTimeString, string credentialScope)
        {
            var canonicalRequest = CanonicalizeRequest(Path, host, dateTimeString, credentialScope);
            var canonicalRequestHashBytes = ComputeHash("SHA-256", canonicalRequest);

            // construct the string to be signed
            var stringToSign = new StringBuilder();
            stringToSign.AppendFormat("{0}-{1}\n{2}\n{3}\n", Scheme, Algorithm, dateTimeString, credentialScope);
            stringToSign.Append(ToHexString(canonicalRequestHashBytes, true));

            var kha = KeyedHashAlgorithm.Create(HmacSha256);
            kha!.Key = DeriveSigningKey(HmacSha256, _credentials.GetCredentials().SecretKey, _region, dateString, Service);

            // compute the final signature for the request, place into the result and return to the 
            // user to be embedded in the request as needed
            var signature = kha.ComputeHash(Encoding.UTF8.GetBytes(stringToSign.ToString()));
            var signatureString = ToHexString(signature, true);
            return signatureString;
        }
        private string CanonicalizeRequest(string path, string host, string dateTimeString, string credentialScope)
        {
            var canonicalRequest = new StringBuilder();
            canonicalRequest.AppendFormat("{0}\n", "GET");
            canonicalRequest.AppendFormat("{0}\n", path);
            canonicalRequest.AppendFormat("{0}\n", GenerateQueryParams(dateTimeString, credentialScope));
            canonicalRequest.AppendFormat("{0}\n", $"host:{host}");
            canonicalRequest.AppendFormat("{0}\n", "");
            canonicalRequest.AppendFormat("{0}\n", "host");
            canonicalRequest.Append(ToHexString(ComputeHash("SHA-256", ""), true));
            return canonicalRequest.ToString();
        }
        private static string ToHexString(byte[] data, bool lowercase)
        {
            var sb = new StringBuilder();
            for (var i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString(lowercase ? "x2" : "X2"));
            }
            return sb.ToString();
        }

        private static byte[] DeriveSigningKey(string algorithm, string awsSecretAccessKey, string region, string date, string service)
        {
            char[] ksecret = (Scheme + awsSecretAccessKey).ToCharArray();
            byte[] hashDate = ComputeKeyedHash(algorithm, Encoding.UTF8.GetBytes(ksecret), Encoding.UTF8.GetBytes(date));
            byte[] hashRegion = ComputeKeyedHash(algorithm, hashDate, Encoding.UTF8.GetBytes(region));
            byte[] hashService = ComputeKeyedHash(algorithm, hashRegion, Encoding.UTF8.GetBytes(service));
            return ComputeKeyedHash(algorithm, hashService, Encoding.UTF8.GetBytes(Terminator));
        }

        private static byte[] ComputeKeyedHash(string algorithm, byte[] key, byte[] data)
        {
            var kha = KeyedHashAlgorithm.Create(algorithm);
            kha!.Key = key;
            return kha.ComputeHash(data);
        }

        private static byte[] ComputeHash(string algorithm, string data)
        {
            return HashAlgorithm.Create(algorithm)!.ComputeHash(Encoding.UTF8.GetBytes(data));

        }
    }
    public class Config
    {
        public string? Language;
        public string MediaEncoding;
        public string SampleRate;
        public string? VocabularyName { get; set; }
        public string? SessionId { get; set; }
        public string? VocabularyFilterName { get; set; }
        public string? VocabularyFilterMethod { get; set; }
        public string? ShowSpeakerLabel { get; set; }
        public string? EnableChannelIdentification { get; set; }
        public string? NumberOfChannels { get; set; }
        public string? EnablePartialResultsStabilization { get; set; }
        public string? PartialResultsStability { get; set; }
        public string? ContentIdentificationType { get; set; }
        public string? ContentRedactionType { get; set; }
        public string? PiiEntityTypes { get; set; }
        public string? LanguageModelName { get; set; }
        public string? IdentifyLanguage { get; set; }
        public string? LanguageOptions { get; set; }
        public string? PreferredLanguage { get; set; }
        public string? VocabularyNames { get; set; }
        public string? VocabularyFilterNames { get; set; }

        public Config(string mediaEncoding, string sampleRate)
        {
            this.MediaEncoding = mediaEncoding;
            this.SampleRate = sampleRate;
        }

        public Config(string mediaEncoding, string sampleRate, string language)
        {
            this.Language = language;
            this.MediaEncoding = mediaEncoding;
            this.SampleRate = sampleRate;
        }

        public SortedDictionary<string, string> GetDictionary()
        {
            SortedDictionary<string, string> dict = new SortedDictionary<string, string>();
            if (!string.IsNullOrEmpty(this.Language)) dict.Add("language-code", this.Language);
            dict.Add("media-encoding", this.MediaEncoding);
            dict.Add("sample-rate", this.SampleRate);
            if (!string.IsNullOrEmpty(this.VocabularyName)) dict.Add("vocabulary-name", this.VocabularyName);
            if (!string.IsNullOrEmpty(this.SessionId)) dict.Add("session-id", this.SessionId);
            if (!string.IsNullOrEmpty(this.VocabularyFilterName)) dict.Add("vocabulary-filter-name", this.VocabularyFilterName);
            if (!string.IsNullOrEmpty(this.VocabularyFilterMethod)) dict.Add("vocabulary-filter-method", this.VocabularyFilterMethod);
            if (!string.IsNullOrEmpty(this.ShowSpeakerLabel)) dict.Add("show-speaker-label", this.ShowSpeakerLabel);
            if (!string.IsNullOrEmpty(this.EnableChannelIdentification)) dict.Add("enable-channel-identification", this.EnableChannelIdentification);
            if (!string.IsNullOrEmpty(this.NumberOfChannels)) dict.Add("number-of-channels", this.NumberOfChannels);
            if (!string.IsNullOrEmpty(this.EnablePartialResultsStabilization)) dict.Add("enable-partial-results-stabilization", this.EnablePartialResultsStabilization);
            if (!string.IsNullOrEmpty(this.PartialResultsStability)) dict.Add("partial-results-stability", this.PartialResultsStability);
            if (!string.IsNullOrEmpty(this.ContentIdentificationType)) dict.Add("content-identification-type", this.ContentIdentificationType);
            if (!string.IsNullOrEmpty(this.ContentRedactionType)) dict.Add("content-redaction-type", this.ContentRedactionType);
            if (!string.IsNullOrEmpty(this.PiiEntityTypes)) dict.Add("pii-entity-types", this.PiiEntityTypes);
            if (!string.IsNullOrEmpty(this.LanguageModelName)) dict.Add("language-model-name", this.LanguageModelName);
            if (!string.IsNullOrEmpty(this.IdentifyLanguage)) dict.Add("identify-language", this.IdentifyLanguage);
            if (!string.IsNullOrEmpty(this.LanguageOptions)) dict.Add("language-options", this.LanguageOptions);
            if (!string.IsNullOrEmpty(this.PreferredLanguage)) dict.Add("preferred-language", this.PreferredLanguage);
            if (!string.IsNullOrEmpty(this.VocabularyNames)) dict.Add("vocabulary-names", this.VocabularyNames);
            if (!string.IsNullOrEmpty(this.VocabularyFilterNames)) dict.Add("vocabulary-filter-names", this.VocabularyFilterNames);

            return dict;
        }
    }
    public class TranscribeException
    {
        public string? ExceptionType { get; set; }
        public string? Message { get; set; }
    }

    public class Alternative
    {
        public List<Item>? Items { get; set; }
        public string? Transcript { get; set; }
    }

    public class Item
    {
        public string? Content { get; set; }
        public double EndTime { get; set; }
        public double StartTime { get; set; }
        public string? Type { get; set; }
        public bool VocabularyFilterMatch { get; set; }
    }

    public class Result
    {
        public List<Alternative>? Alternatives { get; set; }
        public double EndTime { get; set; }
        public bool IsPartial { get; set; }
        public string? ResultId { get; set; }
        public double StartTime { get; set; }
    }

    public class TranscriptEvent
    {
        public Transcript? Transcript { get; set; }
    }

    public class Transcript
    {
        public List<Result>? Results { get; set; }
    }
    #endregion // Models
}
