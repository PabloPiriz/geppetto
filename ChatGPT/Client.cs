﻿using ChatGPT.Models;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using static Shared.Http.HttpExt;

namespace ChatGPT
{
    public class ChatGPTClient : APIClient
    {
        public const string END_POINT = "https://api.openai.com/v1/chat";
        private string ApiKey { get; }
        public ChatGPTClient(HttpClient httpClient, string API_KEY) : base(httpClient, END_POINT)
        {
            ApiKey = API_KEY;
        }

        public async Task<ChatResponse> Chat(Chat chat)
        {
            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", ApiKey);
            try
            {
                var json = JsonSerializer.Serialize(chat, new JsonSerializerOptions { DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull });
                var response = await HttpClient.PostAsync($"{this.BaseUrl}/completions", new StringContent(json, Encoding.UTF8, "application/json"));
                var body = await response.Content.ReadAsStringAsync();
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException(body, null, response.StatusCode);
                }

                return JsonSerializer.Deserialize<ChatResponse>(body) ?? throw new NullReferenceException();
            }
            finally
            {
                HttpClient.DefaultRequestHeaders.Clear();
            }
        }
    }

    public class DummyChatGPTClient : APIClient
    {
        public DummyChatGPTClient(HttpClient httpClient, string API_KEY) : base(httpClient, "")
        {
        }

        public Task<ChatResponse> Chat(Chat chat)
        {
            return Task.FromResult(new ChatResponse()
            {
                Choices = new List<ChatResponse.Choice>
                {
                    new ChatResponse.Choice()
                    {
                        FinishReason = ChatResponse.Choice.FinishReasonType.stop.ToString(),
                        Index = 0,
                        Message = new ChatItem
                        {
                            Role = ChatRoleType.assistant.ToString(),
                            Content = "Service not available",
                        },
                    }
                },
                CreatedUnixUnits = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                Model = chat.Model,
            });
        }
    }
}