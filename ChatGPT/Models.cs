﻿#nullable enable

using System.Text.Json.Serialization;

namespace ChatGPT.Models
{
    public class ChatGPTResponse
    {
        public class Message
        {
            public string text { get; set; } = null!;
        }
        public Message message { get; set; } = null!;
    }
    public enum ChatRoleType
    {
        system,
        user,
        assistant,
        function,
    }
    public class FunctionCall
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = null!;
        [JsonPropertyName("arguments")]
        public string Arguments { get; set; } = null!;
    }
    public class ChatItem
    {
        [JsonIgnore]
        public string Uid { get; } = $"{Guid.NewGuid()}";

        [JsonPropertyName("role")]
        public string Role { get; set; } = null!;
        [JsonPropertyName("content")]
        public string Content { get; set; } = null!;
        [JsonPropertyName("name")]
        public string? Name { get; set; } = null;
        [JsonPropertyName("function_call")]
        public FunctionCall? FunctionCall { get; set; } = null;
    }
    public class Chat
    {
        public const string MODEL_V35 = "gpt-3.5-turbo";
        public const string MODEL_V40 = "gpt-4";

        [JsonPropertyName("model")]
        public string Model { get; set; } = null!;
        [JsonPropertyName("messages")]
        public List<ChatItem> Messages { get; set; } = null!;
        [JsonPropertyName("temperature")]
        public float? Temperature { get; set; } //  [0, 2.0] Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic. We generally recommend altering this or top_p but not both.
        [JsonPropertyName("top_p")]
        public float? TopP { get; set; } // 0.1 means only the tokens comprising the top 10% probability mass are considered
        [JsonPropertyName("n")]
        public int? Alternatives { get; set; } // "n" parameter
        [JsonPropertyName("stream")]
        public bool? Stream { get; set; }
        [JsonPropertyName("stop")]
        public string[]? Stop { get; set; }
        [JsonPropertyName("max_tokens")]
        public int? MaxTokens { get; set; }
        [JsonPropertyName("presence_penalty")]
        public float? PresencePenalty { get; set; } // [-2.0, 2.0] Positive values penalize new tokens based on whether they appear in the text so far
        [JsonPropertyName("frequency_penalty")]
        public float? FrequencyPenalty { get; set; } // [-2.0, 2.0] Positive values penalize new tokens based on their existing frequency in the text so far
        [JsonPropertyName("user")]
        public string? User { get; set; }
    }
    public class ChatResponse
    {
        public class Choice
        {
            public enum FinishReasonType
            {
                stop,
                length,
                function_call,
            }
            [JsonPropertyName("index")]
            public int Index { get; set; }
            [JsonPropertyName("message")]
            public ChatItem Message { get; set; } = null!;
            [JsonPropertyName("finish_reason")]
            public string FinishReason { get; set; } = null!;
        }
        public class UsageStatistics
        {
            [JsonPropertyName("prompt_tokens")]
            public int PromptTokens { get; set; }
            [JsonPropertyName("completion_tokens")]
            public int CompletionTokens { get; set; }
            public int TotalTokens => PromptTokens + CompletionTokens;
        }
        [JsonPropertyName("id")]
        public string ID { get; set; } = null!;
        [JsonPropertyName("object")]
        public string Object { get; set; } = null!;
        [JsonPropertyName("created")]
        public long CreatedUnixUnits { get; set; } // unix timestamp
        public DateTimeOffset CreatedDateTime => DateTimeOffset.FromUnixTimeSeconds(CreatedUnixUnits);
        [JsonPropertyName("model")]
        public string Model { get; set; } = null!;
        [JsonPropertyName("choices")]
        public List<Choice> Choices { get; set; } = null!;
        [JsonPropertyName("usage")]
        public UsageStatistics Usage { get; set; } = null!;
    }
}
