﻿
using AudioSynthesizer;
using Microsoft.CognitiveServices.Speech;
using Shared;
using StoryTelling;
using static StoryTelling.VoiceChatManager.IVoiceChatSettings;

namespace MyApp // Note: actual namespace depends on the project name.
{
    internal class Program
    {
        private const string AzureKey = "";
        public static async Task Main(string[] args)
        {
            var Synthesizer = new AzureSynthesizerClient(Cryptography.Unprotect(AzureKey), "eastus", AzureSynthesizerClient.InputFormatType.Text, SpeechSynthesisOutputFormat.Audio16Khz64KBitRateMonoMp3);
            var voiceID = "es-HN-CarlosNeural";

            var manager = new VoiceChatManager(new VoiceChatSettings()
            {
                VoiceID = voiceID,
                SynthesizeCodec = SynthesizeOutputCodecType.MP3,
            }, Synthesizer
            );
            await manager.InitGPT();
            await manager.InitSynthesize();
            await manager.InitTranscribe();
            await manager.InitAudioCapture();
            await Task.Delay(TimeSpan.FromDays(1));
        }

    }
}