﻿using System.Security.Cryptography;
using System.Text;

namespace Shared
{
    public static class Cryptography
    {
        public static byte[] ComputeSHA256(byte[] data)
        {
            using (var hasher = SHA256.Create())
            {
                return hasher.ComputeHash(data);
            }
        }
        public static byte[] ComputeSHA256(string data, Encoding encoder)
        {
            using (var hasher = SHA256.Create())
            {
                return hasher.ComputeHash(encoder.GetBytes(data));
            }
        }
        public static byte[] ComputeHMACSHA256(byte[] key, byte[] message)
        {
            var hash = new HMACSHA256(key);
            return hash.ComputeHash(message);
        }

        private static string RSA_PUBLIC => Environment.GetEnvironmentVariable("GEPPETTO_RSAPUBLIC") ?? throw new KeyNotFoundException("GEPPETTO_RSAPUBLIC");
        private static string RSA_PRIVATE => Environment.GetEnvironmentVariable("GEPPETTO_RSAPRIVATE") ?? throw new KeyNotFoundException("GEPPETTO_RSAPRIVATE");
        public static byte[] ProtectRaw(byte[] data)
        {
            RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider(2048);
            RSAalg.PersistKeyInCsp = false;
            RSAalg.FromXmlString(RSA_PUBLIC);
            return RSAalg.Encrypt(data, true);
        }
        public static byte[] UnprotectRaw(byte[] data)
        {
            RSACryptoServiceProvider RSAalg = new RSACryptoServiceProvider(2048);
            RSAalg.PersistKeyInCsp = false;
            RSAalg.FromXmlString(RSA_PRIVATE);
            return RSAalg.Decrypt(data, true);
        }
        public static string Protect(string data)
            => Convert.ToBase64String(ProtectRaw(Encoding.UTF32.GetBytes(data)));
        public static string Unprotect(string data)
            => Encoding.UTF32.GetString(UnprotectRaw(Convert.FromBase64String(data)));
    }
}
