﻿#nullable enable

namespace Shared
{
    public static class LinqExt
    {
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (var e in source)
            {
                action(e);
            }
        }

        public static bool TryGetFirst<T>(this IEnumerable<T> stream, Func<T, bool> filter, out T result)
        {
            foreach (var source in stream)
            {
                if (!filter(source)) continue;
                result = source;
                return true;
            }

#pragma warning disable CS8601 // Ignore possible null reference
            result = default;
#pragma warning restore CS8601 // Ignore possible null reference
            return false;
        }

        public static bool TryRemoveFirst<T>(this IList<T> stream, Func<T, bool> filter, out T result)
        {
            for (int i = 0; i < stream.Count; i++)
            {
                var elem = stream[i];
                if (filter(elem))
                {
                    result = elem;
                    stream.RemoveAt(i);
                    return true;
                }
            }
#pragma warning disable CS8601 // Ignore possible null reference
            result = default;
#pragma warning restore CS8601 // Ignore possible null reference
            return false;
        }
    }

}
