﻿#nullable enable

using Shared.Concurrence;

namespace Shared
{
    public static class Logger
    {
        [Flags]
        public enum LoggerFlags
        {
            DEBUG,
            INFO,
            WARNING,
            ERROR,
            CRITICAL
        }
        public static readonly LoggerFlags LOGGER_FLAGS_HIGH = LoggerFlags.WARNING | LoggerFlags.ERROR | LoggerFlags.CRITICAL;
        public static readonly LoggerFlags LOGGER_FLAGS_NORMAL = LoggerFlags.INFO | LOGGER_FLAGS_HIGH;
        public static readonly LoggerFlags LOGGER_FLAGS_ALL = LoggerFlags.DEBUG | LOGGER_FLAGS_NORMAL;
        public static bool SplitMultiline = true;

        private static object? Mutex { get; set; } = null;
        private static bool Locked { get; set; }
        private static LoggerFlags Flags { get; set; } = LOGGER_FLAGS_NORMAL;

        private static AutoLock GetLock() => new AutoLock(Mutex);

        public static void EnableGlobalMutex()
        {
            using (var _lock = GetLock())
            {
                if (Mutex != null)
                    throw new InvalidOperationException();
                Mutex = new object();
            }
        }
        public static void DisableGlobalMutex()
        {
            using (var _lock = GetLock())
            {
                if (Mutex == null)
                    throw new InvalidOperationException();
                Mutex = null;
            }
        }
        public static void SetFlags(LoggerFlags flags)
        {
            using (var _lock = GetLock())
            {
                Flags = flags;
            }
        }


        private static void LogMessae(LoggerFlags flag, TextWriter writer, string lvl, string msg)
        {
            using var _lock = GetLock();
            if (Flags.HasFlag(flag))
            {
                var utcNow = DateTimeOffset.UtcNow;
                var lines = SplitMultiline ? msg.Split("\n") : new[] { msg };
                lines.ForEach(l => writer.WriteLine($"{utcNow:yyyy-MM-dd HH:mm:ss.fff} [{lvl}] {l}"));
            }
        }
        public static void Debug(string str) => LogMessae(LoggerFlags.DEBUG, Console.Out, "DBG", str);
        public static void Info(string str) => LogMessae(LoggerFlags.INFO, Console.Out, "INF", str);
        public static void Warning(string str) => LogMessae(LoggerFlags.WARNING, Console.Out, "WRN", str);
        public static void Error(string str) => LogMessae(LoggerFlags.ERROR, Console.Error, "ERR", str);
        public static void Critical(string str) => LogMessae(LoggerFlags.CRITICAL, Console.Error, "CRT", str);
    }

}