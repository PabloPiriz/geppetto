﻿#nullable enable

using System.Diagnostics;

namespace Shared.Concurrence
{
    public class AutoLock : IDisposable
    {
        private object? Mutex { get; set; }
        public AutoLock(object? mutex)
        {
            Mutex = mutex;
            if (Mutex != null)
                Monitor.Enter(Mutex);
        }

        public void Dispose()
        {
            if (Mutex != null)
            {
                Debug.Assert(Monitor.IsEntered(Mutex));
                Monitor.Exit(Mutex);
            }
        }
    }
}
