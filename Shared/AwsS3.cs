﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.AwsS3
{
    public struct AWSS3Location
    {
        public string Region { get; set; }
        public string Bucket { get; set; }
        public string Key { get; set; }
    }
}
