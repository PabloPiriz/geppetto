﻿#nullable enable

using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Shared.Http
{
    public static class HttpExt
    {
        public static string QueryString(Dictionary<string, string> parameters)
            => QueryString((IEnumerable<KeyValuePair<string, string>>)parameters);
        //=> string.Join('&', parameters.Select(kv => $"{System.Net.WebUtility.UrlEncode(kv.Key)}={System.Net.WebUtility.UrlEncode(kv.Value)}"));
        public static string QueryString(IEnumerable<KeyValuePair<string, string>> parameters)
            => string.Join('&', parameters.Select(kv => $"{System.Net.WebUtility.UrlEncode(kv.Key)}={System.Net.WebUtility.UrlEncode(kv.Value)}"));
        /// <summary>
        /// Use RequestHandlerController attribute in public methods to define endpoints. 
        /// All endpoints must close response resource (see: DisposeWrapper, maybe that help you).
        /// </summary>
        public abstract class HttpServer
        {
            public class RequestHandlerManager
            {
                List<(Regex Rgx, MethodInfo Handler)> GETs = new List<(Regex Rgx, MethodInfo Handler)>();
                List<(Regex Rgx, MethodInfo Handler)> POSTs = new List<(Regex Rgx, MethodInfo Handler)>();
                List<(Regex Rgx, MethodInfo Handler)> DELETEs = new List<(Regex Rgx, MethodInfo Handler)>();
                List<(Regex Rgx, MethodInfo Handler)> ANYs = new List<(Regex Rgx, MethodInfo Handler)>();

                public void Registry(string httpMethod, string rgx, MethodInfo method)
                {
                    switch (httpMethod)
                    {
                        case "GET":
                            GETs.Add((new Regex(rgx, RegexOptions.Compiled), method));
                            break;
                        case "POST":
                            POSTs.Add((new Regex(rgx, RegexOptions.Compiled), method));
                            break;
                        case "DELETE":
                            DELETEs.Add((new Regex(rgx, RegexOptions.Compiled), method));
                            break;
                        case null:
                            ANYs.Add((new Regex(rgx, RegexOptions.Compiled), method));
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                }

                public MethodInfo? GetHandler(string httpMethod, string path)
                {
                    switch (httpMethod)
                    {
                        case "GET":
                            if (GETs.TryGetFirst(h => h.Rgx.IsMatch(path), out var resultGet))
                            {
                                return resultGet.Handler;
                            }
                            break;
                        case "POST":
                            if (POSTs.TryGetFirst(h => h.Rgx.IsMatch(path), out var resultPost))
                            {
                                return resultPost.Handler;
                            }
                            break;
                        case "DELETE":
                            if (DELETEs.TryGetFirst(h => h.Rgx.IsMatch(path), out var resultDelete))
                            {
                                return resultDelete.Handler;
                            }
                            break;
                        default:
                            if (ANYs.TryGetFirst(h => h.Rgx.IsMatch(path), out var resultAny))
                            {
                                return resultAny.Handler;
                            }
                            break;
                    }
                    return null;
                }
            }

            public HttpListener Listener;
            public string Url;

            protected HttpServer(string url)
            {
                this.Url = url;
                Listener = new HttpListener();
                Listener.Prefixes.Add(Url);
            }

            public async Task StartAsync()
            {
                Logger.Info($"Starting server at '{Url}'");
                Listener.Start();
                await HandleIncomingConnections();
            }

            public void Stop()
            {
                Logger.Info($"Stopping server at '{Url}'");
                Listener.Stop();
            }

            private RequestHandlerManager ReqHandlerManager = new RequestHandlerManager();
            protected void LoadHandlers()
            {
                var requestHandlerParameterSequence = new Type[] { typeof(HttpListenerRequest), typeof(HttpListenerResponse) };
                var typeComparer = new BasicTypeComparer();
                foreach (var method in this.GetType().GetMethods())
                {
                    if (!method.IsPublic) continue;
                    if (method.IsConstructor) continue;

                    var metadata = method.GetCustomAttribute<RequestHandlerController>();
                    if (metadata?.PathRgx == null) continue;
                    if (!method.GetParameters().Select(p => p.ParameterType).SequenceEqual(requestHandlerParameterSequence, typeComparer)) continue;

                    Logger.Info($"Loading handler: [{metadata.Method} - {metadata.PathRgx}] {method.Name}");
                    ReqHandlerManager.Registry(metadata.Method, metadata.PathRgx, method);
                }
            }

            protected async Task HandleIncomingConnections()
            {

                // While a user hasn't visited the `shutdown` url, keep on handling requests
                while (true)
                {
                    if (!Listener.IsListening)
                    {
                        break;
                    }
                    // Will wait here until we hear from a connection
                    HttpListenerContext ctx = await Listener.GetContextAsync();

                    var req = ctx.Request;
                    var resp = ctx.Response;

                    if (req.Url == null)
                    {
                        Logger.Info($"Ignore request without url.");
                        continue;
                    }

                    var handler = ReqHandlerManager.GetHandler(req.HttpMethod, req.Url.AbsolutePath);
                    if (handler != null)
                    {
                        handler.Invoke(this, new object[] { req, resp });
                    }
                }
                Listener.Stop();
            }
        }

        public class RequestHandlerController : Attribute
        {
            internal class Comparer : IEqualityComparer<RequestHandlerController>
            {
                public bool Equals(RequestHandlerController? x, RequestHandlerController? y)
                {
                    return x?.Method == y?.Method
                        && x?.PathRgx == y?.PathRgx;
                }

                public int GetHashCode([DisallowNull] RequestHandlerController obj)
                {
                    throw new NotImplementedException();
                }
            }
            public string Method { get; }
            public string PathRgx { get; }

            public RequestHandlerController(string method, string path)
            {
                this.Method = method;
                this.PathRgx = path;
            }
        }

        public abstract class APIClient
        {
            protected readonly HttpClient HttpClient;
            protected string BaseUrl;
            protected Dictionary<string, string> Headers;

            protected APIClient(HttpClient httpClient, string baseUrl)
            {
                this.HttpClient = httpClient;
                this.BaseUrl = baseUrl.TrimEnd('/');
                this.Headers = new Dictionary<string, string>();
            }
        }
    }

    public class BasicTypeComparer : IEqualityComparer<Type>
    {
        public bool Equals(Type? x, Type? y)
        {
            return x?.Assembly == y?.Assembly &&
                x?.Namespace == y?.Namespace &&
                x?.Name == y?.Name;
        }

        public int GetHashCode(Type obj)
        {
            throw new NotImplementedException();
        }
    }

}
