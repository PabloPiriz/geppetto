﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public static class Util
    {
        public static async Task<string?> ReadLineAsync(CancellationToken cancellationToken = default)
        {
            return await Task.Run(() => Console.ReadLine(), cancellationToken);
        }
    }
}
